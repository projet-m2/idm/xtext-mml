/*
 * generated by Xtext 2.19.0
 */
package fr.istic.serializer;

import com.google.inject.Inject;
import fr.istic.mML.AllVariables;
import fr.istic.mML.CSVSeparator;
import fr.istic.mML.CrossValidation;
import fr.istic.mML.DataInput;
import fr.istic.mML.DefaultTarget;
import fr.istic.mML.Evaluation;
import fr.istic.mML.Item;
import fr.istic.mML.LinearRegression;
import fr.istic.mML.MLChoice;
import fr.istic.mML.MMLModel;
import fr.istic.mML.MMLPackage;
import fr.istic.mML.NamedTarget;
import fr.istic.mML.PredictorVariables;
import fr.istic.mML.RegressionTree;
import fr.istic.mML.SimpleSplit;
import fr.istic.mML.Variables;
import fr.istic.services.MMLGrammarAccess;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class MMLSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private MMLGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == MMLPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case MMLPackage.ALL_VARIABLES:
				sequence_AllVariables(context, (AllVariables) semanticObject); 
				return; 
			case MMLPackage.CSV_SEPARATOR:
				sequence_CSVSeparator(context, (CSVSeparator) semanticObject); 
				return; 
			case MMLPackage.CROSS_VALIDATION:
				sequence_CrossValidation(context, (CrossValidation) semanticObject); 
				return; 
			case MMLPackage.DATA_INPUT:
				sequence_DataInput(context, (DataInput) semanticObject); 
				return; 
			case MMLPackage.DEFAULT_TARGET:
				sequence_DefaultTarget(context, (DefaultTarget) semanticObject); 
				return; 
			case MMLPackage.EVALUATION:
				sequence_Evaluation(context, (Evaluation) semanticObject); 
				return; 
			case MMLPackage.ITEM:
				sequence_Item(context, (Item) semanticObject); 
				return; 
			case MMLPackage.LINEAR_REGRESSION:
				sequence_LinearRegression(context, (LinearRegression) semanticObject); 
				return; 
			case MMLPackage.ML_CHOICE:
				sequence_MLChoice(context, (MLChoice) semanticObject); 
				return; 
			case MMLPackage.MML_MODEL:
				sequence_MMLModel(context, (MMLModel) semanticObject); 
				return; 
			case MMLPackage.NAMED_TARGET:
				sequence_NamedTarget(context, (NamedTarget) semanticObject); 
				return; 
			case MMLPackage.PREDICTOR_VARIABLES:
				sequence_PredictorVariables(context, (PredictorVariables) semanticObject); 
				return; 
			case MMLPackage.REGRESSION_TREE:
				sequence_RegressionTree(context, (RegressionTree) semanticObject); 
				return; 
			case MMLPackage.SIMPLE_SPLIT:
				sequence_SimpleSplit(context, (SimpleSplit) semanticObject); 
				return; 
			case MMLPackage.VARIABLES:
				sequence_Variables(context, (Variables) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Predictions returns AllVariables
	 *     AllVariables returns AllVariables
	 *
	 * Constraint:
	 *     all='all'
	 */
	protected void sequence_AllVariables(ISerializationContext context, AllVariables semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.ALL_VARIABLES__ALL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.ALL_VARIABLES__ALL));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAllVariablesAccess().getAllAllKeyword_0(), semanticObject.getAll());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     CSVSeparator returns CSVSeparator
	 *
	 * Constraint:
	 *     separator=STRING
	 */
	protected void sequence_CSVSeparator(ISerializationContext context, CSVSeparator semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.CSV_SEPARATOR__SEPARATOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.CSV_SEPARATOR__SEPARATOR));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getCSVSeparatorAccess().getSeparatorSTRINGTerminalRuleCall_1_0(), semanticObject.getSeparator());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     StrategyEvaluation returns CrossValidation
	 *     CrossValidation returns CrossValidation
	 *
	 * Constraint:
	 *     number=INT
	 */
	protected void sequence_CrossValidation(ISerializationContext context, CrossValidation semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.CROSS_VALIDATION__NUMBER) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.CROSS_VALIDATION__NUMBER));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getCrossValidationAccess().getNumberINTTerminalRuleCall_2_0(), semanticObject.getNumber());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     DataInput returns DataInput
	 *
	 * Constraint:
	 *     (file=STRING csvSeparator=CSVSeparator?)
	 */
	protected void sequence_DataInput(ISerializationContext context, DataInput semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Target returns DefaultTarget
	 *     DefaultTarget returns DefaultTarget
	 *
	 * Constraint:
	 *     default='default'
	 */
	protected void sequence_DefaultTarget(ISerializationContext context, DefaultTarget semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.DEFAULT_TARGET__DEFAULT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.DEFAULT_TARGET__DEFAULT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0(), semanticObject.getDefault());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Evaluation returns Evaluation
	 *
	 * Constraint:
	 *     strategyEvaluation=StrategyEvaluation
	 */
	protected void sequence_Evaluation(ISerializationContext context, Evaluation semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.EVALUATION__STRATEGY_EVALUATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.EVALUATION__STRATEGY_EVALUATION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getEvaluationAccess().getStrategyEvaluationStrategyEvaluationParserRuleCall_1_0(), semanticObject.getStrategyEvaluation());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Item returns Item
	 *
	 * Constraint:
	 *     columnName=STRING
	 */
	protected void sequence_Item(ISerializationContext context, Item semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.ITEM__COLUMN_NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.ITEM__COLUMN_NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getItemAccess().getColumnNameSTRINGTerminalRuleCall_0(), semanticObject.getColumnName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     MLAlgorithm returns LinearRegression
	 *     LinearRegression returns LinearRegression
	 *
	 * Constraint:
	 *     linearRegression='LinearRegression'
	 */
	protected void sequence_LinearRegression(ISerializationContext context, LinearRegression semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.LINEAR_REGRESSION__LINEAR_REGRESSION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.LINEAR_REGRESSION__LINEAR_REGRESSION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0(), semanticObject.getLinearRegression());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     MLChoice returns MLChoice
	 *
	 * Constraint:
	 *     (framework=MLFramework algorithm=MLAlgorithm)
	 */
	protected void sequence_MLChoice(ISerializationContext context, MLChoice semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.ML_CHOICE__FRAMEWORK) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.ML_CHOICE__FRAMEWORK));
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.ML_CHOICE__ALGORITHM) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.ML_CHOICE__ALGORITHM));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getMLChoiceAccess().getFrameworkMLFrameworkEnumRuleCall_1_0(), semanticObject.getFramework());
		feeder.accept(grammarAccess.getMLChoiceAccess().getAlgorithmMLAlgorithmParserRuleCall_4_0(), semanticObject.getAlgorithm());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     MMLModel returns MMLModel
	 *
	 * Constraint:
	 *     (input=DataInput mlChoice=MLChoice variables=Variables evaluation=Evaluation)
	 */
	protected void sequence_MMLModel(ISerializationContext context, MMLModel semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.MML_MODEL__INPUT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.MML_MODEL__INPUT));
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.MML_MODEL__ML_CHOICE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.MML_MODEL__ML_CHOICE));
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.MML_MODEL__VARIABLES) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.MML_MODEL__VARIABLES));
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.MML_MODEL__EVALUATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.MML_MODEL__EVALUATION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getMMLModelAccess().getInputDataInputParserRuleCall_0_0(), semanticObject.getInput());
		feeder.accept(grammarAccess.getMMLModelAccess().getMlChoiceMLChoiceParserRuleCall_1_0(), semanticObject.getMlChoice());
		feeder.accept(grammarAccess.getMMLModelAccess().getVariablesVariablesParserRuleCall_2_0(), semanticObject.getVariables());
		feeder.accept(grammarAccess.getMMLModelAccess().getEvaluationEvaluationParserRuleCall_3_0(), semanticObject.getEvaluation());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Target returns NamedTarget
	 *     NamedTarget returns NamedTarget
	 *
	 * Constraint:
	 *     target=Item
	 */
	protected void sequence_NamedTarget(ISerializationContext context, NamedTarget semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.NAMED_TARGET__TARGET) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.NAMED_TARGET__TARGET));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getNamedTargetAccess().getTargetItemParserRuleCall_0(), semanticObject.getTarget());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Predictions returns PredictorVariables
	 *     PredictorVariables returns PredictorVariables
	 *
	 * Constraint:
	 *     (vars+=Item vars+=Item*)
	 */
	protected void sequence_PredictorVariables(ISerializationContext context, PredictorVariables semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     MLAlgorithm returns RegressionTree
	 *     RegressionTree returns RegressionTree
	 *
	 * Constraint:
	 *     regressionTree='RegressionTree'
	 */
	protected void sequence_RegressionTree(ISerializationContext context, RegressionTree semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.REGRESSION_TREE__REGRESSION_TREE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.REGRESSION_TREE__REGRESSION_TREE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0(), semanticObject.getRegressionTree());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     StrategyEvaluation returns SimpleSplit
	 *     SimpleSplit returns SimpleSplit
	 *
	 * Constraint:
	 *     validationPercent=INT
	 */
	protected void sequence_SimpleSplit(ISerializationContext context, SimpleSplit semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.SIMPLE_SPLIT__VALIDATION_PERCENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.SIMPLE_SPLIT__VALIDATION_PERCENT));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSimpleSplitAccess().getValidationPercentINTTerminalRuleCall_2_0(), semanticObject.getValidationPercent());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Variables returns Variables
	 *
	 * Constraint:
	 *     (predictions=Predictions target=Target)
	 */
	protected void sequence_Variables(ISerializationContext context, Variables semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.VARIABLES__PREDICTIONS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.VARIABLES__PREDICTIONS));
			if (transientValues.isValueTransient(semanticObject, MMLPackage.Literals.VARIABLES__TARGET) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MMLPackage.Literals.VARIABLES__TARGET));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getVariablesAccess().getPredictionsPredictionsParserRuleCall_2_0(), semanticObject.getPredictions());
		feeder.accept(grammarAccess.getVariablesAccess().getTargetTargetParserRuleCall_5_0(), semanticObject.getTarget());
		feeder.finish();
	}
	
	
}
