package fr.istic.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.istic.services.MMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMMLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "','", "';'", "'\\n'", "'dataSet'", "'separator'", "'mlFramework'", "'mlAlgorithm'", "'RegressionTree'", "'LinearRegression'", "'variables'", "'prediction'", "'target'", "'default'", "'all'", "'+'", "'evaluation'", "'TrainingSet'", "'validationPercent'", "'CrossValidation'", "'partitionCount'", "'Scikit-learn'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMML.g"; }



     	private MMLGrammarAccess grammarAccess;

        public InternalMMLParser(TokenStream input, MMLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "MMLModel";
       	}

       	@Override
       	protected MMLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleMMLModel"
    // InternalMML.g:65:1: entryRuleMMLModel returns [EObject current=null] : iv_ruleMMLModel= ruleMMLModel EOF ;
    public final EObject entryRuleMMLModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMMLModel = null;


        try {
            // InternalMML.g:65:49: (iv_ruleMMLModel= ruleMMLModel EOF )
            // InternalMML.g:66:2: iv_ruleMMLModel= ruleMMLModel EOF
            {
             newCompositeNode(grammarAccess.getMMLModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMMLModel=ruleMMLModel();

            state._fsp--;

             current =iv_ruleMMLModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMMLModel"


    // $ANTLR start "ruleMMLModel"
    // InternalMML.g:72:1: ruleMMLModel returns [EObject current=null] : ( ( (lv_input_0_0= ruleDataInput ) ) ( (lv_mlChoice_1_0= ruleMLChoice ) ) ( (lv_variables_2_0= ruleVariables ) ) ( (lv_evaluation_3_0= ruleEvaluation ) ) ) ;
    public final EObject ruleMMLModel() throws RecognitionException {
        EObject current = null;

        EObject lv_input_0_0 = null;

        EObject lv_mlChoice_1_0 = null;

        EObject lv_variables_2_0 = null;

        EObject lv_evaluation_3_0 = null;



        	enterRule();

        try {
            // InternalMML.g:78:2: ( ( ( (lv_input_0_0= ruleDataInput ) ) ( (lv_mlChoice_1_0= ruleMLChoice ) ) ( (lv_variables_2_0= ruleVariables ) ) ( (lv_evaluation_3_0= ruleEvaluation ) ) ) )
            // InternalMML.g:79:2: ( ( (lv_input_0_0= ruleDataInput ) ) ( (lv_mlChoice_1_0= ruleMLChoice ) ) ( (lv_variables_2_0= ruleVariables ) ) ( (lv_evaluation_3_0= ruleEvaluation ) ) )
            {
            // InternalMML.g:79:2: ( ( (lv_input_0_0= ruleDataInput ) ) ( (lv_mlChoice_1_0= ruleMLChoice ) ) ( (lv_variables_2_0= ruleVariables ) ) ( (lv_evaluation_3_0= ruleEvaluation ) ) )
            // InternalMML.g:80:3: ( (lv_input_0_0= ruleDataInput ) ) ( (lv_mlChoice_1_0= ruleMLChoice ) ) ( (lv_variables_2_0= ruleVariables ) ) ( (lv_evaluation_3_0= ruleEvaluation ) )
            {
            // InternalMML.g:80:3: ( (lv_input_0_0= ruleDataInput ) )
            // InternalMML.g:81:4: (lv_input_0_0= ruleDataInput )
            {
            // InternalMML.g:81:4: (lv_input_0_0= ruleDataInput )
            // InternalMML.g:82:5: lv_input_0_0= ruleDataInput
            {

            					newCompositeNode(grammarAccess.getMMLModelAccess().getInputDataInputParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_3);
            lv_input_0_0=ruleDataInput();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMMLModelRule());
            					}
            					set(
            						current,
            						"input",
            						lv_input_0_0,
            						"fr.istic.MML.DataInput");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMML.g:99:3: ( (lv_mlChoice_1_0= ruleMLChoice ) )
            // InternalMML.g:100:4: (lv_mlChoice_1_0= ruleMLChoice )
            {
            // InternalMML.g:100:4: (lv_mlChoice_1_0= ruleMLChoice )
            // InternalMML.g:101:5: lv_mlChoice_1_0= ruleMLChoice
            {

            					newCompositeNode(grammarAccess.getMMLModelAccess().getMlChoiceMLChoiceParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_mlChoice_1_0=ruleMLChoice();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMMLModelRule());
            					}
            					set(
            						current,
            						"mlChoice",
            						lv_mlChoice_1_0,
            						"fr.istic.MML.MLChoice");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMML.g:118:3: ( (lv_variables_2_0= ruleVariables ) )
            // InternalMML.g:119:4: (lv_variables_2_0= ruleVariables )
            {
            // InternalMML.g:119:4: (lv_variables_2_0= ruleVariables )
            // InternalMML.g:120:5: lv_variables_2_0= ruleVariables
            {

            					newCompositeNode(grammarAccess.getMMLModelAccess().getVariablesVariablesParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_variables_2_0=ruleVariables();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMMLModelRule());
            					}
            					set(
            						current,
            						"variables",
            						lv_variables_2_0,
            						"fr.istic.MML.Variables");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMML.g:137:3: ( (lv_evaluation_3_0= ruleEvaluation ) )
            // InternalMML.g:138:4: (lv_evaluation_3_0= ruleEvaluation )
            {
            // InternalMML.g:138:4: (lv_evaluation_3_0= ruleEvaluation )
            // InternalMML.g:139:5: lv_evaluation_3_0= ruleEvaluation
            {

            					newCompositeNode(grammarAccess.getMMLModelAccess().getEvaluationEvaluationParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_evaluation_3_0=ruleEvaluation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMMLModelRule());
            					}
            					set(
            						current,
            						"evaluation",
            						lv_evaluation_3_0,
            						"fr.istic.MML.Evaluation");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMMLModel"


    // $ANTLR start "ruleCOMMA"
    // InternalMML.g:161:1: ruleCOMMA returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= ',' ;
    public final AntlrDatatypeRuleToken ruleCOMMA() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMML.g:167:2: (kw= ',' )
            // InternalMML.g:168:2: kw= ','
            {
            kw=(Token)match(input,11,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getCOMMAAccess().getCommaKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCOMMA"


    // $ANTLR start "ruleSEMICOLON"
    // InternalMML.g:177:1: ruleSEMICOLON returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= ';' ;
    public final AntlrDatatypeRuleToken ruleSEMICOLON() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMML.g:183:2: (kw= ';' )
            // InternalMML.g:184:2: kw= ';'
            {
            kw=(Token)match(input,12,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getSEMICOLONAccess().getSemicolonKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSEMICOLON"


    // $ANTLR start "ruleNEWLINE"
    // InternalMML.g:193:1: ruleNEWLINE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '\\n' ;
    public final AntlrDatatypeRuleToken ruleNEWLINE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMML.g:199:2: (kw= '\\n' )
            // InternalMML.g:200:2: kw= '\\n'
            {
            kw=(Token)match(input,13,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getNEWLINEAccess().getLineFeedKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNEWLINE"


    // $ANTLR start "entryRuleDataInput"
    // InternalMML.g:208:1: entryRuleDataInput returns [EObject current=null] : iv_ruleDataInput= ruleDataInput EOF ;
    public final EObject entryRuleDataInput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataInput = null;


        try {
            // InternalMML.g:208:50: (iv_ruleDataInput= ruleDataInput EOF )
            // InternalMML.g:209:2: iv_ruleDataInput= ruleDataInput EOF
            {
             newCompositeNode(grammarAccess.getDataInputRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataInput=ruleDataInput();

            state._fsp--;

             current =iv_ruleDataInput; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataInput"


    // $ANTLR start "ruleDataInput"
    // InternalMML.g:215:1: ruleDataInput returns [EObject current=null] : (otherlv_0= 'dataSet' ( (lv_file_1_0= RULE_STRING ) ) ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )? ruleSEMICOLON ruleNEWLINE ) ;
    public final EObject ruleDataInput() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_file_1_0=null;
        EObject lv_csvSeparator_2_0 = null;



        	enterRule();

        try {
            // InternalMML.g:221:2: ( (otherlv_0= 'dataSet' ( (lv_file_1_0= RULE_STRING ) ) ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )? ruleSEMICOLON ruleNEWLINE ) )
            // InternalMML.g:222:2: (otherlv_0= 'dataSet' ( (lv_file_1_0= RULE_STRING ) ) ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )? ruleSEMICOLON ruleNEWLINE )
            {
            // InternalMML.g:222:2: (otherlv_0= 'dataSet' ( (lv_file_1_0= RULE_STRING ) ) ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )? ruleSEMICOLON ruleNEWLINE )
            // InternalMML.g:223:3: otherlv_0= 'dataSet' ( (lv_file_1_0= RULE_STRING ) ) ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )? ruleSEMICOLON ruleNEWLINE
            {
            otherlv_0=(Token)match(input,14,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getDataInputAccess().getDataSetKeyword_0());
            		
            // InternalMML.g:227:3: ( (lv_file_1_0= RULE_STRING ) )
            // InternalMML.g:228:4: (lv_file_1_0= RULE_STRING )
            {
            // InternalMML.g:228:4: (lv_file_1_0= RULE_STRING )
            // InternalMML.g:229:5: lv_file_1_0= RULE_STRING
            {
            lv_file_1_0=(Token)match(input,RULE_STRING,FOLLOW_7); 

            					newLeafNode(lv_file_1_0, grammarAccess.getDataInputAccess().getFileSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDataInputRule());
            					}
            					setWithLastConsumed(
            						current,
            						"file",
            						lv_file_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalMML.g:245:3: ( (lv_csvSeparator_2_0= ruleCSVSeparator ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==15) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalMML.g:246:4: (lv_csvSeparator_2_0= ruleCSVSeparator )
                    {
                    // InternalMML.g:246:4: (lv_csvSeparator_2_0= ruleCSVSeparator )
                    // InternalMML.g:247:5: lv_csvSeparator_2_0= ruleCSVSeparator
                    {

                    					newCompositeNode(grammarAccess.getDataInputAccess().getCsvSeparatorCSVSeparatorParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_7);
                    lv_csvSeparator_2_0=ruleCSVSeparator();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataInputRule());
                    					}
                    					set(
                    						current,
                    						"csvSeparator",
                    						lv_csvSeparator_2_0,
                    						"fr.istic.MML.CSVSeparator");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            			newCompositeNode(grammarAccess.getDataInputAccess().getSEMICOLONParserRuleCall_3());
            		
            pushFollow(FOLLOW_8);
            ruleSEMICOLON();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            			newCompositeNode(grammarAccess.getDataInputAccess().getNEWLINEParserRuleCall_4());
            		
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataInput"


    // $ANTLR start "entryRuleCSVSeparator"
    // InternalMML.g:282:1: entryRuleCSVSeparator returns [EObject current=null] : iv_ruleCSVSeparator= ruleCSVSeparator EOF ;
    public final EObject entryRuleCSVSeparator() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCSVSeparator = null;


        try {
            // InternalMML.g:282:53: (iv_ruleCSVSeparator= ruleCSVSeparator EOF )
            // InternalMML.g:283:2: iv_ruleCSVSeparator= ruleCSVSeparator EOF
            {
             newCompositeNode(grammarAccess.getCSVSeparatorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCSVSeparator=ruleCSVSeparator();

            state._fsp--;

             current =iv_ruleCSVSeparator; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCSVSeparator"


    // $ANTLR start "ruleCSVSeparator"
    // InternalMML.g:289:1: ruleCSVSeparator returns [EObject current=null] : (otherlv_0= 'separator' ( (lv_separator_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleCSVSeparator() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_separator_1_0=null;


        	enterRule();

        try {
            // InternalMML.g:295:2: ( (otherlv_0= 'separator' ( (lv_separator_1_0= RULE_STRING ) ) ) )
            // InternalMML.g:296:2: (otherlv_0= 'separator' ( (lv_separator_1_0= RULE_STRING ) ) )
            {
            // InternalMML.g:296:2: (otherlv_0= 'separator' ( (lv_separator_1_0= RULE_STRING ) ) )
            // InternalMML.g:297:3: otherlv_0= 'separator' ( (lv_separator_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getCSVSeparatorAccess().getSeparatorKeyword_0());
            		
            // InternalMML.g:301:3: ( (lv_separator_1_0= RULE_STRING ) )
            // InternalMML.g:302:4: (lv_separator_1_0= RULE_STRING )
            {
            // InternalMML.g:302:4: (lv_separator_1_0= RULE_STRING )
            // InternalMML.g:303:5: lv_separator_1_0= RULE_STRING
            {
            lv_separator_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_separator_1_0, grammarAccess.getCSVSeparatorAccess().getSeparatorSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCSVSeparatorRule());
            					}
            					setWithLastConsumed(
            						current,
            						"separator",
            						lv_separator_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCSVSeparator"


    // $ANTLR start "entryRuleMLChoice"
    // InternalMML.g:323:1: entryRuleMLChoice returns [EObject current=null] : iv_ruleMLChoice= ruleMLChoice EOF ;
    public final EObject entryRuleMLChoice() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMLChoice = null;


        try {
            // InternalMML.g:323:49: (iv_ruleMLChoice= ruleMLChoice EOF )
            // InternalMML.g:324:2: iv_ruleMLChoice= ruleMLChoice EOF
            {
             newCompositeNode(grammarAccess.getMLChoiceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMLChoice=ruleMLChoice();

            state._fsp--;

             current =iv_ruleMLChoice; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMLChoice"


    // $ANTLR start "ruleMLChoice"
    // InternalMML.g:330:1: ruleMLChoice returns [EObject current=null] : (otherlv_0= 'mlFramework' ( (lv_framework_1_0= ruleMLFramework ) ) ruleCOMMA otherlv_3= 'mlAlgorithm' ( (lv_algorithm_4_0= ruleMLAlgorithm ) ) ruleSEMICOLON ruleNEWLINE ) ;
    public final EObject ruleMLChoice() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Enumerator lv_framework_1_0 = null;

        EObject lv_algorithm_4_0 = null;



        	enterRule();

        try {
            // InternalMML.g:336:2: ( (otherlv_0= 'mlFramework' ( (lv_framework_1_0= ruleMLFramework ) ) ruleCOMMA otherlv_3= 'mlAlgorithm' ( (lv_algorithm_4_0= ruleMLAlgorithm ) ) ruleSEMICOLON ruleNEWLINE ) )
            // InternalMML.g:337:2: (otherlv_0= 'mlFramework' ( (lv_framework_1_0= ruleMLFramework ) ) ruleCOMMA otherlv_3= 'mlAlgorithm' ( (lv_algorithm_4_0= ruleMLAlgorithm ) ) ruleSEMICOLON ruleNEWLINE )
            {
            // InternalMML.g:337:2: (otherlv_0= 'mlFramework' ( (lv_framework_1_0= ruleMLFramework ) ) ruleCOMMA otherlv_3= 'mlAlgorithm' ( (lv_algorithm_4_0= ruleMLAlgorithm ) ) ruleSEMICOLON ruleNEWLINE )
            // InternalMML.g:338:3: otherlv_0= 'mlFramework' ( (lv_framework_1_0= ruleMLFramework ) ) ruleCOMMA otherlv_3= 'mlAlgorithm' ( (lv_algorithm_4_0= ruleMLAlgorithm ) ) ruleSEMICOLON ruleNEWLINE
            {
            otherlv_0=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getMLChoiceAccess().getMlFrameworkKeyword_0());
            		
            // InternalMML.g:342:3: ( (lv_framework_1_0= ruleMLFramework ) )
            // InternalMML.g:343:4: (lv_framework_1_0= ruleMLFramework )
            {
            // InternalMML.g:343:4: (lv_framework_1_0= ruleMLFramework )
            // InternalMML.g:344:5: lv_framework_1_0= ruleMLFramework
            {

            					newCompositeNode(grammarAccess.getMLChoiceAccess().getFrameworkMLFrameworkEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_10);
            lv_framework_1_0=ruleMLFramework();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMLChoiceRule());
            					}
            					set(
            						current,
            						"framework",
            						lv_framework_1_0,
            						"fr.istic.MML.MLFramework");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            			newCompositeNode(grammarAccess.getMLChoiceAccess().getCOMMAParserRuleCall_2());
            		
            pushFollow(FOLLOW_11);
            ruleCOMMA();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            otherlv_3=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_3, grammarAccess.getMLChoiceAccess().getMlAlgorithmKeyword_3());
            		
            // InternalMML.g:372:3: ( (lv_algorithm_4_0= ruleMLAlgorithm ) )
            // InternalMML.g:373:4: (lv_algorithm_4_0= ruleMLAlgorithm )
            {
            // InternalMML.g:373:4: (lv_algorithm_4_0= ruleMLAlgorithm )
            // InternalMML.g:374:5: lv_algorithm_4_0= ruleMLAlgorithm
            {

            					newCompositeNode(grammarAccess.getMLChoiceAccess().getAlgorithmMLAlgorithmParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_7);
            lv_algorithm_4_0=ruleMLAlgorithm();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMLChoiceRule());
            					}
            					set(
            						current,
            						"algorithm",
            						lv_algorithm_4_0,
            						"fr.istic.MML.MLAlgorithm");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            			newCompositeNode(grammarAccess.getMLChoiceAccess().getSEMICOLONParserRuleCall_5());
            		
            pushFollow(FOLLOW_8);
            ruleSEMICOLON();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            			newCompositeNode(grammarAccess.getMLChoiceAccess().getNEWLINEParserRuleCall_6());
            		
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMLChoice"


    // $ANTLR start "entryRuleMLAlgorithm"
    // InternalMML.g:409:1: entryRuleMLAlgorithm returns [EObject current=null] : iv_ruleMLAlgorithm= ruleMLAlgorithm EOF ;
    public final EObject entryRuleMLAlgorithm() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMLAlgorithm = null;


        try {
            // InternalMML.g:409:52: (iv_ruleMLAlgorithm= ruleMLAlgorithm EOF )
            // InternalMML.g:410:2: iv_ruleMLAlgorithm= ruleMLAlgorithm EOF
            {
             newCompositeNode(grammarAccess.getMLAlgorithmRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMLAlgorithm=ruleMLAlgorithm();

            state._fsp--;

             current =iv_ruleMLAlgorithm; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMLAlgorithm"


    // $ANTLR start "ruleMLAlgorithm"
    // InternalMML.g:416:1: ruleMLAlgorithm returns [EObject current=null] : (this_RegressionTree_0= ruleRegressionTree | this_LinearRegression_1= ruleLinearRegression ) ;
    public final EObject ruleMLAlgorithm() throws RecognitionException {
        EObject current = null;

        EObject this_RegressionTree_0 = null;

        EObject this_LinearRegression_1 = null;



        	enterRule();

        try {
            // InternalMML.g:422:2: ( (this_RegressionTree_0= ruleRegressionTree | this_LinearRegression_1= ruleLinearRegression ) )
            // InternalMML.g:423:2: (this_RegressionTree_0= ruleRegressionTree | this_LinearRegression_1= ruleLinearRegression )
            {
            // InternalMML.g:423:2: (this_RegressionTree_0= ruleRegressionTree | this_LinearRegression_1= ruleLinearRegression )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==18) ) {
                alt2=1;
            }
            else if ( (LA2_0==19) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMML.g:424:3: this_RegressionTree_0= ruleRegressionTree
                    {

                    			newCompositeNode(grammarAccess.getMLAlgorithmAccess().getRegressionTreeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_RegressionTree_0=ruleRegressionTree();

                    state._fsp--;


                    			current = this_RegressionTree_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMML.g:433:3: this_LinearRegression_1= ruleLinearRegression
                    {

                    			newCompositeNode(grammarAccess.getMLAlgorithmAccess().getLinearRegressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_LinearRegression_1=ruleLinearRegression();

                    state._fsp--;


                    			current = this_LinearRegression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMLAlgorithm"


    // $ANTLR start "entryRuleRegressionTree"
    // InternalMML.g:445:1: entryRuleRegressionTree returns [EObject current=null] : iv_ruleRegressionTree= ruleRegressionTree EOF ;
    public final EObject entryRuleRegressionTree() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRegressionTree = null;


        try {
            // InternalMML.g:445:55: (iv_ruleRegressionTree= ruleRegressionTree EOF )
            // InternalMML.g:446:2: iv_ruleRegressionTree= ruleRegressionTree EOF
            {
             newCompositeNode(grammarAccess.getRegressionTreeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRegressionTree=ruleRegressionTree();

            state._fsp--;

             current =iv_ruleRegressionTree; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRegressionTree"


    // $ANTLR start "ruleRegressionTree"
    // InternalMML.g:452:1: ruleRegressionTree returns [EObject current=null] : ( (lv_regressionTree_0_0= 'RegressionTree' ) ) ;
    public final EObject ruleRegressionTree() throws RecognitionException {
        EObject current = null;

        Token lv_regressionTree_0_0=null;


        	enterRule();

        try {
            // InternalMML.g:458:2: ( ( (lv_regressionTree_0_0= 'RegressionTree' ) ) )
            // InternalMML.g:459:2: ( (lv_regressionTree_0_0= 'RegressionTree' ) )
            {
            // InternalMML.g:459:2: ( (lv_regressionTree_0_0= 'RegressionTree' ) )
            // InternalMML.g:460:3: (lv_regressionTree_0_0= 'RegressionTree' )
            {
            // InternalMML.g:460:3: (lv_regressionTree_0_0= 'RegressionTree' )
            // InternalMML.g:461:4: lv_regressionTree_0_0= 'RegressionTree'
            {
            lv_regressionTree_0_0=(Token)match(input,18,FOLLOW_2); 

            				newLeafNode(lv_regressionTree_0_0, grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getRegressionTreeRule());
            				}
            				setWithLastConsumed(current, "regressionTree", lv_regressionTree_0_0, "RegressionTree");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRegressionTree"


    // $ANTLR start "entryRuleLinearRegression"
    // InternalMML.g:476:1: entryRuleLinearRegression returns [EObject current=null] : iv_ruleLinearRegression= ruleLinearRegression EOF ;
    public final EObject entryRuleLinearRegression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLinearRegression = null;


        try {
            // InternalMML.g:476:57: (iv_ruleLinearRegression= ruleLinearRegression EOF )
            // InternalMML.g:477:2: iv_ruleLinearRegression= ruleLinearRegression EOF
            {
             newCompositeNode(grammarAccess.getLinearRegressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLinearRegression=ruleLinearRegression();

            state._fsp--;

             current =iv_ruleLinearRegression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLinearRegression"


    // $ANTLR start "ruleLinearRegression"
    // InternalMML.g:483:1: ruleLinearRegression returns [EObject current=null] : ( (lv_linearRegression_0_0= 'LinearRegression' ) ) ;
    public final EObject ruleLinearRegression() throws RecognitionException {
        EObject current = null;

        Token lv_linearRegression_0_0=null;


        	enterRule();

        try {
            // InternalMML.g:489:2: ( ( (lv_linearRegression_0_0= 'LinearRegression' ) ) )
            // InternalMML.g:490:2: ( (lv_linearRegression_0_0= 'LinearRegression' ) )
            {
            // InternalMML.g:490:2: ( (lv_linearRegression_0_0= 'LinearRegression' ) )
            // InternalMML.g:491:3: (lv_linearRegression_0_0= 'LinearRegression' )
            {
            // InternalMML.g:491:3: (lv_linearRegression_0_0= 'LinearRegression' )
            // InternalMML.g:492:4: lv_linearRegression_0_0= 'LinearRegression'
            {
            lv_linearRegression_0_0=(Token)match(input,19,FOLLOW_2); 

            				newLeafNode(lv_linearRegression_0_0, grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getLinearRegressionRule());
            				}
            				setWithLastConsumed(current, "linearRegression", lv_linearRegression_0_0, "LinearRegression");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLinearRegression"


    // $ANTLR start "entryRuleVariables"
    // InternalMML.g:507:1: entryRuleVariables returns [EObject current=null] : iv_ruleVariables= ruleVariables EOF ;
    public final EObject entryRuleVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariables = null;


        try {
            // InternalMML.g:507:50: (iv_ruleVariables= ruleVariables EOF )
            // InternalMML.g:508:2: iv_ruleVariables= ruleVariables EOF
            {
             newCompositeNode(grammarAccess.getVariablesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariables=ruleVariables();

            state._fsp--;

             current =iv_ruleVariables; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariables"


    // $ANTLR start "ruleVariables"
    // InternalMML.g:514:1: ruleVariables returns [EObject current=null] : (otherlv_0= 'variables' otherlv_1= 'prediction' ( (lv_predictions_2_0= rulePredictions ) ) ruleCOMMA otherlv_4= 'target' ( (lv_target_5_0= ruleTarget ) ) ruleSEMICOLON ruleNEWLINE ) ;
    public final EObject ruleVariables() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_4=null;
        EObject lv_predictions_2_0 = null;

        EObject lv_target_5_0 = null;



        	enterRule();

        try {
            // InternalMML.g:520:2: ( (otherlv_0= 'variables' otherlv_1= 'prediction' ( (lv_predictions_2_0= rulePredictions ) ) ruleCOMMA otherlv_4= 'target' ( (lv_target_5_0= ruleTarget ) ) ruleSEMICOLON ruleNEWLINE ) )
            // InternalMML.g:521:2: (otherlv_0= 'variables' otherlv_1= 'prediction' ( (lv_predictions_2_0= rulePredictions ) ) ruleCOMMA otherlv_4= 'target' ( (lv_target_5_0= ruleTarget ) ) ruleSEMICOLON ruleNEWLINE )
            {
            // InternalMML.g:521:2: (otherlv_0= 'variables' otherlv_1= 'prediction' ( (lv_predictions_2_0= rulePredictions ) ) ruleCOMMA otherlv_4= 'target' ( (lv_target_5_0= ruleTarget ) ) ruleSEMICOLON ruleNEWLINE )
            // InternalMML.g:522:3: otherlv_0= 'variables' otherlv_1= 'prediction' ( (lv_predictions_2_0= rulePredictions ) ) ruleCOMMA otherlv_4= 'target' ( (lv_target_5_0= ruleTarget ) ) ruleSEMICOLON ruleNEWLINE
            {
            otherlv_0=(Token)match(input,20,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getVariablesAccess().getVariablesKeyword_0());
            		
            otherlv_1=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getVariablesAccess().getPredictionKeyword_1());
            		
            // InternalMML.g:530:3: ( (lv_predictions_2_0= rulePredictions ) )
            // InternalMML.g:531:4: (lv_predictions_2_0= rulePredictions )
            {
            // InternalMML.g:531:4: (lv_predictions_2_0= rulePredictions )
            // InternalMML.g:532:5: lv_predictions_2_0= rulePredictions
            {

            					newCompositeNode(grammarAccess.getVariablesAccess().getPredictionsPredictionsParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_10);
            lv_predictions_2_0=rulePredictions();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVariablesRule());
            					}
            					set(
            						current,
            						"predictions",
            						lv_predictions_2_0,
            						"fr.istic.MML.Predictions");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            			newCompositeNode(grammarAccess.getVariablesAccess().getCOMMAParserRuleCall_3());
            		
            pushFollow(FOLLOW_15);
            ruleCOMMA();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            otherlv_4=(Token)match(input,22,FOLLOW_16); 

            			newLeafNode(otherlv_4, grammarAccess.getVariablesAccess().getTargetKeyword_4());
            		
            // InternalMML.g:560:3: ( (lv_target_5_0= ruleTarget ) )
            // InternalMML.g:561:4: (lv_target_5_0= ruleTarget )
            {
            // InternalMML.g:561:4: (lv_target_5_0= ruleTarget )
            // InternalMML.g:562:5: lv_target_5_0= ruleTarget
            {

            					newCompositeNode(grammarAccess.getVariablesAccess().getTargetTargetParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_7);
            lv_target_5_0=ruleTarget();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVariablesRule());
            					}
            					set(
            						current,
            						"target",
            						lv_target_5_0,
            						"fr.istic.MML.Target");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            			newCompositeNode(grammarAccess.getVariablesAccess().getSEMICOLONParserRuleCall_6());
            		
            pushFollow(FOLLOW_8);
            ruleSEMICOLON();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            			newCompositeNode(grammarAccess.getVariablesAccess().getNEWLINEParserRuleCall_7());
            		
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariables"


    // $ANTLR start "entryRuleTarget"
    // InternalMML.g:597:1: entryRuleTarget returns [EObject current=null] : iv_ruleTarget= ruleTarget EOF ;
    public final EObject entryRuleTarget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTarget = null;


        try {
            // InternalMML.g:597:47: (iv_ruleTarget= ruleTarget EOF )
            // InternalMML.g:598:2: iv_ruleTarget= ruleTarget EOF
            {
             newCompositeNode(grammarAccess.getTargetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTarget=ruleTarget();

            state._fsp--;

             current =iv_ruleTarget; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTarget"


    // $ANTLR start "ruleTarget"
    // InternalMML.g:604:1: ruleTarget returns [EObject current=null] : (this_DefaultTarget_0= ruleDefaultTarget | this_NamedTarget_1= ruleNamedTarget ) ;
    public final EObject ruleTarget() throws RecognitionException {
        EObject current = null;

        EObject this_DefaultTarget_0 = null;

        EObject this_NamedTarget_1 = null;



        	enterRule();

        try {
            // InternalMML.g:610:2: ( (this_DefaultTarget_0= ruleDefaultTarget | this_NamedTarget_1= ruleNamedTarget ) )
            // InternalMML.g:611:2: (this_DefaultTarget_0= ruleDefaultTarget | this_NamedTarget_1= ruleNamedTarget )
            {
            // InternalMML.g:611:2: (this_DefaultTarget_0= ruleDefaultTarget | this_NamedTarget_1= ruleNamedTarget )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==23) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_STRING) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMML.g:612:3: this_DefaultTarget_0= ruleDefaultTarget
                    {

                    			newCompositeNode(grammarAccess.getTargetAccess().getDefaultTargetParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DefaultTarget_0=ruleDefaultTarget();

                    state._fsp--;


                    			current = this_DefaultTarget_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMML.g:621:3: this_NamedTarget_1= ruleNamedTarget
                    {

                    			newCompositeNode(grammarAccess.getTargetAccess().getNamedTargetParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_NamedTarget_1=ruleNamedTarget();

                    state._fsp--;


                    			current = this_NamedTarget_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTarget"


    // $ANTLR start "entryRuleDefaultTarget"
    // InternalMML.g:633:1: entryRuleDefaultTarget returns [EObject current=null] : iv_ruleDefaultTarget= ruleDefaultTarget EOF ;
    public final EObject entryRuleDefaultTarget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDefaultTarget = null;


        try {
            // InternalMML.g:633:54: (iv_ruleDefaultTarget= ruleDefaultTarget EOF )
            // InternalMML.g:634:2: iv_ruleDefaultTarget= ruleDefaultTarget EOF
            {
             newCompositeNode(grammarAccess.getDefaultTargetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDefaultTarget=ruleDefaultTarget();

            state._fsp--;

             current =iv_ruleDefaultTarget; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDefaultTarget"


    // $ANTLR start "ruleDefaultTarget"
    // InternalMML.g:640:1: ruleDefaultTarget returns [EObject current=null] : ( (lv_default_0_0= 'default' ) ) ;
    public final EObject ruleDefaultTarget() throws RecognitionException {
        EObject current = null;

        Token lv_default_0_0=null;


        	enterRule();

        try {
            // InternalMML.g:646:2: ( ( (lv_default_0_0= 'default' ) ) )
            // InternalMML.g:647:2: ( (lv_default_0_0= 'default' ) )
            {
            // InternalMML.g:647:2: ( (lv_default_0_0= 'default' ) )
            // InternalMML.g:648:3: (lv_default_0_0= 'default' )
            {
            // InternalMML.g:648:3: (lv_default_0_0= 'default' )
            // InternalMML.g:649:4: lv_default_0_0= 'default'
            {
            lv_default_0_0=(Token)match(input,23,FOLLOW_2); 

            				newLeafNode(lv_default_0_0, grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getDefaultTargetRule());
            				}
            				setWithLastConsumed(current, "default", lv_default_0_0, "default");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDefaultTarget"


    // $ANTLR start "entryRuleNamedTarget"
    // InternalMML.g:664:1: entryRuleNamedTarget returns [EObject current=null] : iv_ruleNamedTarget= ruleNamedTarget EOF ;
    public final EObject entryRuleNamedTarget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNamedTarget = null;


        try {
            // InternalMML.g:664:52: (iv_ruleNamedTarget= ruleNamedTarget EOF )
            // InternalMML.g:665:2: iv_ruleNamedTarget= ruleNamedTarget EOF
            {
             newCompositeNode(grammarAccess.getNamedTargetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNamedTarget=ruleNamedTarget();

            state._fsp--;

             current =iv_ruleNamedTarget; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNamedTarget"


    // $ANTLR start "ruleNamedTarget"
    // InternalMML.g:671:1: ruleNamedTarget returns [EObject current=null] : ( (lv_target_0_0= ruleItem ) ) ;
    public final EObject ruleNamedTarget() throws RecognitionException {
        EObject current = null;

        EObject lv_target_0_0 = null;



        	enterRule();

        try {
            // InternalMML.g:677:2: ( ( (lv_target_0_0= ruleItem ) ) )
            // InternalMML.g:678:2: ( (lv_target_0_0= ruleItem ) )
            {
            // InternalMML.g:678:2: ( (lv_target_0_0= ruleItem ) )
            // InternalMML.g:679:3: (lv_target_0_0= ruleItem )
            {
            // InternalMML.g:679:3: (lv_target_0_0= ruleItem )
            // InternalMML.g:680:4: lv_target_0_0= ruleItem
            {

            				newCompositeNode(grammarAccess.getNamedTargetAccess().getTargetItemParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_target_0_0=ruleItem();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getNamedTargetRule());
            				}
            				set(
            					current,
            					"target",
            					lv_target_0_0,
            					"fr.istic.MML.Item");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNamedTarget"


    // $ANTLR start "entryRulePredictions"
    // InternalMML.g:700:1: entryRulePredictions returns [EObject current=null] : iv_rulePredictions= rulePredictions EOF ;
    public final EObject entryRulePredictions() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredictions = null;


        try {
            // InternalMML.g:700:52: (iv_rulePredictions= rulePredictions EOF )
            // InternalMML.g:701:2: iv_rulePredictions= rulePredictions EOF
            {
             newCompositeNode(grammarAccess.getPredictionsRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePredictions=rulePredictions();

            state._fsp--;

             current =iv_rulePredictions; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredictions"


    // $ANTLR start "rulePredictions"
    // InternalMML.g:707:1: rulePredictions returns [EObject current=null] : (this_AllVariables_0= ruleAllVariables | this_PredictorVariables_1= rulePredictorVariables ) ;
    public final EObject rulePredictions() throws RecognitionException {
        EObject current = null;

        EObject this_AllVariables_0 = null;

        EObject this_PredictorVariables_1 = null;



        	enterRule();

        try {
            // InternalMML.g:713:2: ( (this_AllVariables_0= ruleAllVariables | this_PredictorVariables_1= rulePredictorVariables ) )
            // InternalMML.g:714:2: (this_AllVariables_0= ruleAllVariables | this_PredictorVariables_1= rulePredictorVariables )
            {
            // InternalMML.g:714:2: (this_AllVariables_0= ruleAllVariables | this_PredictorVariables_1= rulePredictorVariables )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==24) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_STRING) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMML.g:715:3: this_AllVariables_0= ruleAllVariables
                    {

                    			newCompositeNode(grammarAccess.getPredictionsAccess().getAllVariablesParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_AllVariables_0=ruleAllVariables();

                    state._fsp--;


                    			current = this_AllVariables_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMML.g:724:3: this_PredictorVariables_1= rulePredictorVariables
                    {

                    			newCompositeNode(grammarAccess.getPredictionsAccess().getPredictorVariablesParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_PredictorVariables_1=rulePredictorVariables();

                    state._fsp--;


                    			current = this_PredictorVariables_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredictions"


    // $ANTLR start "entryRuleAllVariables"
    // InternalMML.g:736:1: entryRuleAllVariables returns [EObject current=null] : iv_ruleAllVariables= ruleAllVariables EOF ;
    public final EObject entryRuleAllVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAllVariables = null;


        try {
            // InternalMML.g:736:53: (iv_ruleAllVariables= ruleAllVariables EOF )
            // InternalMML.g:737:2: iv_ruleAllVariables= ruleAllVariables EOF
            {
             newCompositeNode(grammarAccess.getAllVariablesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAllVariables=ruleAllVariables();

            state._fsp--;

             current =iv_ruleAllVariables; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAllVariables"


    // $ANTLR start "ruleAllVariables"
    // InternalMML.g:743:1: ruleAllVariables returns [EObject current=null] : ( (lv_all_0_0= 'all' ) ) ;
    public final EObject ruleAllVariables() throws RecognitionException {
        EObject current = null;

        Token lv_all_0_0=null;


        	enterRule();

        try {
            // InternalMML.g:749:2: ( ( (lv_all_0_0= 'all' ) ) )
            // InternalMML.g:750:2: ( (lv_all_0_0= 'all' ) )
            {
            // InternalMML.g:750:2: ( (lv_all_0_0= 'all' ) )
            // InternalMML.g:751:3: (lv_all_0_0= 'all' )
            {
            // InternalMML.g:751:3: (lv_all_0_0= 'all' )
            // InternalMML.g:752:4: lv_all_0_0= 'all'
            {
            lv_all_0_0=(Token)match(input,24,FOLLOW_2); 

            				newLeafNode(lv_all_0_0, grammarAccess.getAllVariablesAccess().getAllAllKeyword_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getAllVariablesRule());
            				}
            				setWithLastConsumed(current, "all", lv_all_0_0, "all");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAllVariables"


    // $ANTLR start "entryRulePredictorVariables"
    // InternalMML.g:767:1: entryRulePredictorVariables returns [EObject current=null] : iv_rulePredictorVariables= rulePredictorVariables EOF ;
    public final EObject entryRulePredictorVariables() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredictorVariables = null;


        try {
            // InternalMML.g:767:59: (iv_rulePredictorVariables= rulePredictorVariables EOF )
            // InternalMML.g:768:2: iv_rulePredictorVariables= rulePredictorVariables EOF
            {
             newCompositeNode(grammarAccess.getPredictorVariablesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePredictorVariables=rulePredictorVariables();

            state._fsp--;

             current =iv_rulePredictorVariables; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredictorVariables"


    // $ANTLR start "rulePredictorVariables"
    // InternalMML.g:774:1: rulePredictorVariables returns [EObject current=null] : ( ( (lv_vars_0_0= ruleItem ) ) (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )* ) ;
    public final EObject rulePredictorVariables() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_vars_0_0 = null;

        EObject lv_vars_2_0 = null;



        	enterRule();

        try {
            // InternalMML.g:780:2: ( ( ( (lv_vars_0_0= ruleItem ) ) (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )* ) )
            // InternalMML.g:781:2: ( ( (lv_vars_0_0= ruleItem ) ) (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )* )
            {
            // InternalMML.g:781:2: ( ( (lv_vars_0_0= ruleItem ) ) (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )* )
            // InternalMML.g:782:3: ( (lv_vars_0_0= ruleItem ) ) (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )*
            {
            // InternalMML.g:782:3: ( (lv_vars_0_0= ruleItem ) )
            // InternalMML.g:783:4: (lv_vars_0_0= ruleItem )
            {
            // InternalMML.g:783:4: (lv_vars_0_0= ruleItem )
            // InternalMML.g:784:5: lv_vars_0_0= ruleItem
            {

            					newCompositeNode(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_17);
            lv_vars_0_0=ruleItem();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPredictorVariablesRule());
            					}
            					add(
            						current,
            						"vars",
            						lv_vars_0_0,
            						"fr.istic.MML.Item");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMML.g:801:3: (otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==25) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMML.g:802:4: otherlv_1= '+' ( (lv_vars_2_0= ruleItem ) )
            	    {
            	    otherlv_1=(Token)match(input,25,FOLLOW_14); 

            	    				newLeafNode(otherlv_1, grammarAccess.getPredictorVariablesAccess().getPlusSignKeyword_1_0());
            	    			
            	    // InternalMML.g:806:4: ( (lv_vars_2_0= ruleItem ) )
            	    // InternalMML.g:807:5: (lv_vars_2_0= ruleItem )
            	    {
            	    // InternalMML.g:807:5: (lv_vars_2_0= ruleItem )
            	    // InternalMML.g:808:6: lv_vars_2_0= ruleItem
            	    {

            	    						newCompositeNode(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_vars_2_0=ruleItem();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPredictorVariablesRule());
            	    						}
            	    						add(
            	    							current,
            	    							"vars",
            	    							lv_vars_2_0,
            	    							"fr.istic.MML.Item");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredictorVariables"


    // $ANTLR start "entryRuleItem"
    // InternalMML.g:830:1: entryRuleItem returns [EObject current=null] : iv_ruleItem= ruleItem EOF ;
    public final EObject entryRuleItem() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleItem = null;


        try {
            // InternalMML.g:830:45: (iv_ruleItem= ruleItem EOF )
            // InternalMML.g:831:2: iv_ruleItem= ruleItem EOF
            {
             newCompositeNode(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleItem=ruleItem();

            state._fsp--;

             current =iv_ruleItem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalMML.g:837:1: ruleItem returns [EObject current=null] : ( (lv_columnName_0_0= RULE_STRING ) ) ;
    public final EObject ruleItem() throws RecognitionException {
        EObject current = null;

        Token lv_columnName_0_0=null;


        	enterRule();

        try {
            // InternalMML.g:843:2: ( ( (lv_columnName_0_0= RULE_STRING ) ) )
            // InternalMML.g:844:2: ( (lv_columnName_0_0= RULE_STRING ) )
            {
            // InternalMML.g:844:2: ( (lv_columnName_0_0= RULE_STRING ) )
            // InternalMML.g:845:3: (lv_columnName_0_0= RULE_STRING )
            {
            // InternalMML.g:845:3: (lv_columnName_0_0= RULE_STRING )
            // InternalMML.g:846:4: lv_columnName_0_0= RULE_STRING
            {
            lv_columnName_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_columnName_0_0, grammarAccess.getItemAccess().getColumnNameSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getItemRule());
            				}
            				setWithLastConsumed(
            					current,
            					"columnName",
            					lv_columnName_0_0,
            					"org.eclipse.xtext.common.Terminals.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleEvaluation"
    // InternalMML.g:865:1: entryRuleEvaluation returns [EObject current=null] : iv_ruleEvaluation= ruleEvaluation EOF ;
    public final EObject entryRuleEvaluation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvaluation = null;


        try {
            // InternalMML.g:865:51: (iv_ruleEvaluation= ruleEvaluation EOF )
            // InternalMML.g:866:2: iv_ruleEvaluation= ruleEvaluation EOF
            {
             newCompositeNode(grammarAccess.getEvaluationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvaluation=ruleEvaluation();

            state._fsp--;

             current =iv_ruleEvaluation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvaluation"


    // $ANTLR start "ruleEvaluation"
    // InternalMML.g:872:1: ruleEvaluation returns [EObject current=null] : (otherlv_0= 'evaluation' ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) ) ruleSEMICOLON ( ruleNEWLINE )? ) ;
    public final EObject ruleEvaluation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_strategyEvaluation_1_0 = null;



        	enterRule();

        try {
            // InternalMML.g:878:2: ( (otherlv_0= 'evaluation' ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) ) ruleSEMICOLON ( ruleNEWLINE )? ) )
            // InternalMML.g:879:2: (otherlv_0= 'evaluation' ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) ) ruleSEMICOLON ( ruleNEWLINE )? )
            {
            // InternalMML.g:879:2: (otherlv_0= 'evaluation' ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) ) ruleSEMICOLON ( ruleNEWLINE )? )
            // InternalMML.g:880:3: otherlv_0= 'evaluation' ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) ) ruleSEMICOLON ( ruleNEWLINE )?
            {
            otherlv_0=(Token)match(input,26,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getEvaluationAccess().getEvaluationKeyword_0());
            		
            // InternalMML.g:884:3: ( (lv_strategyEvaluation_1_0= ruleStrategyEvaluation ) )
            // InternalMML.g:885:4: (lv_strategyEvaluation_1_0= ruleStrategyEvaluation )
            {
            // InternalMML.g:885:4: (lv_strategyEvaluation_1_0= ruleStrategyEvaluation )
            // InternalMML.g:886:5: lv_strategyEvaluation_1_0= ruleStrategyEvaluation
            {

            					newCompositeNode(grammarAccess.getEvaluationAccess().getStrategyEvaluationStrategyEvaluationParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_7);
            lv_strategyEvaluation_1_0=ruleStrategyEvaluation();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEvaluationRule());
            					}
            					set(
            						current,
            						"strategyEvaluation",
            						lv_strategyEvaluation_1_0,
            						"fr.istic.MML.StrategyEvaluation");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            			newCompositeNode(grammarAccess.getEvaluationAccess().getSEMICOLONParserRuleCall_2());
            		
            pushFollow(FOLLOW_19);
            ruleSEMICOLON();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            // InternalMML.g:910:3: ( ruleNEWLINE )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==13) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMML.g:911:4: ruleNEWLINE
                    {

                    				newCompositeNode(grammarAccess.getEvaluationAccess().getNEWLINEParserRuleCall_3());
                    			
                    pushFollow(FOLLOW_2);
                    ruleNEWLINE();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvaluation"


    // $ANTLR start "entryRuleStrategyEvaluation"
    // InternalMML.g:923:1: entryRuleStrategyEvaluation returns [EObject current=null] : iv_ruleStrategyEvaluation= ruleStrategyEvaluation EOF ;
    public final EObject entryRuleStrategyEvaluation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStrategyEvaluation = null;


        try {
            // InternalMML.g:923:59: (iv_ruleStrategyEvaluation= ruleStrategyEvaluation EOF )
            // InternalMML.g:924:2: iv_ruleStrategyEvaluation= ruleStrategyEvaluation EOF
            {
             newCompositeNode(grammarAccess.getStrategyEvaluationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStrategyEvaluation=ruleStrategyEvaluation();

            state._fsp--;

             current =iv_ruleStrategyEvaluation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStrategyEvaluation"


    // $ANTLR start "ruleStrategyEvaluation"
    // InternalMML.g:930:1: ruleStrategyEvaluation returns [EObject current=null] : (this_SimpleSplit_0= ruleSimpleSplit | this_CrossValidation_1= ruleCrossValidation ) ;
    public final EObject ruleStrategyEvaluation() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleSplit_0 = null;

        EObject this_CrossValidation_1 = null;



        	enterRule();

        try {
            // InternalMML.g:936:2: ( (this_SimpleSplit_0= ruleSimpleSplit | this_CrossValidation_1= ruleCrossValidation ) )
            // InternalMML.g:937:2: (this_SimpleSplit_0= ruleSimpleSplit | this_CrossValidation_1= ruleCrossValidation )
            {
            // InternalMML.g:937:2: (this_SimpleSplit_0= ruleSimpleSplit | this_CrossValidation_1= ruleCrossValidation )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==27) ) {
                alt7=1;
            }
            else if ( (LA7_0==29) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalMML.g:938:3: this_SimpleSplit_0= ruleSimpleSplit
                    {

                    			newCompositeNode(grammarAccess.getStrategyEvaluationAccess().getSimpleSplitParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleSplit_0=ruleSimpleSplit();

                    state._fsp--;


                    			current = this_SimpleSplit_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMML.g:947:3: this_CrossValidation_1= ruleCrossValidation
                    {

                    			newCompositeNode(grammarAccess.getStrategyEvaluationAccess().getCrossValidationParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CrossValidation_1=ruleCrossValidation();

                    state._fsp--;


                    			current = this_CrossValidation_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStrategyEvaluation"


    // $ANTLR start "entryRuleSimpleSplit"
    // InternalMML.g:959:1: entryRuleSimpleSplit returns [EObject current=null] : iv_ruleSimpleSplit= ruleSimpleSplit EOF ;
    public final EObject entryRuleSimpleSplit() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleSplit = null;


        try {
            // InternalMML.g:959:52: (iv_ruleSimpleSplit= ruleSimpleSplit EOF )
            // InternalMML.g:960:2: iv_ruleSimpleSplit= ruleSimpleSplit EOF
            {
             newCompositeNode(grammarAccess.getSimpleSplitRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleSplit=ruleSimpleSplit();

            state._fsp--;

             current =iv_ruleSimpleSplit; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleSplit"


    // $ANTLR start "ruleSimpleSplit"
    // InternalMML.g:966:1: ruleSimpleSplit returns [EObject current=null] : (otherlv_0= 'TrainingSet' otherlv_1= 'validationPercent' ( (lv_validationPercent_2_0= RULE_INT ) ) ) ;
    public final EObject ruleSimpleSplit() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_validationPercent_2_0=null;


        	enterRule();

        try {
            // InternalMML.g:972:2: ( (otherlv_0= 'TrainingSet' otherlv_1= 'validationPercent' ( (lv_validationPercent_2_0= RULE_INT ) ) ) )
            // InternalMML.g:973:2: (otherlv_0= 'TrainingSet' otherlv_1= 'validationPercent' ( (lv_validationPercent_2_0= RULE_INT ) ) )
            {
            // InternalMML.g:973:2: (otherlv_0= 'TrainingSet' otherlv_1= 'validationPercent' ( (lv_validationPercent_2_0= RULE_INT ) ) )
            // InternalMML.g:974:3: otherlv_0= 'TrainingSet' otherlv_1= 'validationPercent' ( (lv_validationPercent_2_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getSimpleSplitAccess().getTrainingSetKeyword_0());
            		
            otherlv_1=(Token)match(input,28,FOLLOW_21); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleSplitAccess().getValidationPercentKeyword_1());
            		
            // InternalMML.g:982:3: ( (lv_validationPercent_2_0= RULE_INT ) )
            // InternalMML.g:983:4: (lv_validationPercent_2_0= RULE_INT )
            {
            // InternalMML.g:983:4: (lv_validationPercent_2_0= RULE_INT )
            // InternalMML.g:984:5: lv_validationPercent_2_0= RULE_INT
            {
            lv_validationPercent_2_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            					newLeafNode(lv_validationPercent_2_0, grammarAccess.getSimpleSplitAccess().getValidationPercentINTTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleSplitRule());
            					}
            					setWithLastConsumed(
            						current,
            						"validationPercent",
            						lv_validationPercent_2_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleSplit"


    // $ANTLR start "entryRuleCrossValidation"
    // InternalMML.g:1004:1: entryRuleCrossValidation returns [EObject current=null] : iv_ruleCrossValidation= ruleCrossValidation EOF ;
    public final EObject entryRuleCrossValidation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCrossValidation = null;


        try {
            // InternalMML.g:1004:56: (iv_ruleCrossValidation= ruleCrossValidation EOF )
            // InternalMML.g:1005:2: iv_ruleCrossValidation= ruleCrossValidation EOF
            {
             newCompositeNode(grammarAccess.getCrossValidationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCrossValidation=ruleCrossValidation();

            state._fsp--;

             current =iv_ruleCrossValidation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCrossValidation"


    // $ANTLR start "ruleCrossValidation"
    // InternalMML.g:1011:1: ruleCrossValidation returns [EObject current=null] : (otherlv_0= 'CrossValidation' otherlv_1= 'partitionCount' ( (lv_number_2_0= RULE_INT ) ) ) ;
    public final EObject ruleCrossValidation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_number_2_0=null;


        	enterRule();

        try {
            // InternalMML.g:1017:2: ( (otherlv_0= 'CrossValidation' otherlv_1= 'partitionCount' ( (lv_number_2_0= RULE_INT ) ) ) )
            // InternalMML.g:1018:2: (otherlv_0= 'CrossValidation' otherlv_1= 'partitionCount' ( (lv_number_2_0= RULE_INT ) ) )
            {
            // InternalMML.g:1018:2: (otherlv_0= 'CrossValidation' otherlv_1= 'partitionCount' ( (lv_number_2_0= RULE_INT ) ) )
            // InternalMML.g:1019:3: otherlv_0= 'CrossValidation' otherlv_1= 'partitionCount' ( (lv_number_2_0= RULE_INT ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_22); 

            			newLeafNode(otherlv_0, grammarAccess.getCrossValidationAccess().getCrossValidationKeyword_0());
            		
            otherlv_1=(Token)match(input,30,FOLLOW_21); 

            			newLeafNode(otherlv_1, grammarAccess.getCrossValidationAccess().getPartitionCountKeyword_1());
            		
            // InternalMML.g:1027:3: ( (lv_number_2_0= RULE_INT ) )
            // InternalMML.g:1028:4: (lv_number_2_0= RULE_INT )
            {
            // InternalMML.g:1028:4: (lv_number_2_0= RULE_INT )
            // InternalMML.g:1029:5: lv_number_2_0= RULE_INT
            {
            lv_number_2_0=(Token)match(input,RULE_INT,FOLLOW_2); 

            					newLeafNode(lv_number_2_0, grammarAccess.getCrossValidationAccess().getNumberINTTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCrossValidationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"number",
            						lv_number_2_0,
            						"org.eclipse.xtext.common.Terminals.INT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCrossValidation"


    // $ANTLR start "ruleMLFramework"
    // InternalMML.g:1049:1: ruleMLFramework returns [Enumerator current=null] : (enumLiteral_0= 'Scikit-learn' ) ;
    public final Enumerator ruleMLFramework() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalMML.g:1055:2: ( (enumLiteral_0= 'Scikit-learn' ) )
            // InternalMML.g:1056:2: (enumLiteral_0= 'Scikit-learn' )
            {
            // InternalMML.g:1056:2: (enumLiteral_0= 'Scikit-learn' )
            // InternalMML.g:1057:3: enumLiteral_0= 'Scikit-learn'
            {
            enumLiteral_0=(Token)match(input,31,FOLLOW_2); 

            			current = grammarAccess.getMLFrameworkAccess().getSCIKITLEARNEnumLiteralDeclaration().getEnumLiteral().getInstance();
            			newLeafNode(enumLiteral_0, grammarAccess.getMLFrameworkAccess().getSCIKITLEARNEnumLiteralDeclaration());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMLFramework"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000009000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000001800010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000028000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000040000000L});

}