/**
 * generated by Xtext 2.19.0
 */
package fr.istic;

import fr.istic.MMLStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class MMLStandaloneSetup extends MMLStandaloneSetupGenerated {
  public static void doSetup() {
    new MMLStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
