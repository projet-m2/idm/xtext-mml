/*
 * generated by Xtext 2.19.0
 */
package fr.istic.ide.contentassist.antlr;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.istic.ide.contentassist.antlr.internal.InternalMMLParser;
import fr.istic.services.MMLGrammarAccess;
import java.util.Map;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class MMLParser extends AbstractContentAssistParser {

	@Singleton
	public static final class NameMappings {
		
		private final Map<AbstractElement, String> mappings;
		
		@Inject
		public NameMappings(MMLGrammarAccess grammarAccess) {
			ImmutableMap.Builder<AbstractElement, String> builder = ImmutableMap.builder();
			init(builder, grammarAccess);
			this.mappings = builder.build();
		}
		
		public String getRuleName(AbstractElement element) {
			return mappings.get(element);
		}
		
		private static void init(ImmutableMap.Builder<AbstractElement, String> builder, MMLGrammarAccess grammarAccess) {
			builder.put(grammarAccess.getMLAlgorithmAccess().getAlternatives(), "rule__MLAlgorithm__Alternatives");
			builder.put(grammarAccess.getTargetAccess().getAlternatives(), "rule__Target__Alternatives");
			builder.put(grammarAccess.getPredictionsAccess().getAlternatives(), "rule__Predictions__Alternatives");
			builder.put(grammarAccess.getStrategyEvaluationAccess().getAlternatives(), "rule__StrategyEvaluation__Alternatives");
			builder.put(grammarAccess.getMMLModelAccess().getGroup(), "rule__MMLModel__Group__0");
			builder.put(grammarAccess.getDataInputAccess().getGroup(), "rule__DataInput__Group__0");
			builder.put(grammarAccess.getCSVSeparatorAccess().getGroup(), "rule__CSVSeparator__Group__0");
			builder.put(grammarAccess.getMLChoiceAccess().getGroup(), "rule__MLChoice__Group__0");
			builder.put(grammarAccess.getVariablesAccess().getGroup(), "rule__Variables__Group__0");
			builder.put(grammarAccess.getPredictorVariablesAccess().getGroup(), "rule__PredictorVariables__Group__0");
			builder.put(grammarAccess.getPredictorVariablesAccess().getGroup_1(), "rule__PredictorVariables__Group_1__0");
			builder.put(grammarAccess.getEvaluationAccess().getGroup(), "rule__Evaluation__Group__0");
			builder.put(grammarAccess.getSimpleSplitAccess().getGroup(), "rule__SimpleSplit__Group__0");
			builder.put(grammarAccess.getCrossValidationAccess().getGroup(), "rule__CrossValidation__Group__0");
			builder.put(grammarAccess.getMMLModelAccess().getInputAssignment_0(), "rule__MMLModel__InputAssignment_0");
			builder.put(grammarAccess.getMMLModelAccess().getMlChoiceAssignment_1(), "rule__MMLModel__MlChoiceAssignment_1");
			builder.put(grammarAccess.getMMLModelAccess().getVariablesAssignment_2(), "rule__MMLModel__VariablesAssignment_2");
			builder.put(grammarAccess.getMMLModelAccess().getEvaluationAssignment_3(), "rule__MMLModel__EvaluationAssignment_3");
			builder.put(grammarAccess.getDataInputAccess().getFileAssignment_1(), "rule__DataInput__FileAssignment_1");
			builder.put(grammarAccess.getDataInputAccess().getCsvSeparatorAssignment_2(), "rule__DataInput__CsvSeparatorAssignment_2");
			builder.put(grammarAccess.getCSVSeparatorAccess().getSeparatorAssignment_1(), "rule__CSVSeparator__SeparatorAssignment_1");
			builder.put(grammarAccess.getMLChoiceAccess().getFrameworkAssignment_1(), "rule__MLChoice__FrameworkAssignment_1");
			builder.put(grammarAccess.getMLChoiceAccess().getAlgorithmAssignment_4(), "rule__MLChoice__AlgorithmAssignment_4");
			builder.put(grammarAccess.getRegressionTreeAccess().getRegressionTreeAssignment(), "rule__RegressionTree__RegressionTreeAssignment");
			builder.put(grammarAccess.getLinearRegressionAccess().getLinearRegressionAssignment(), "rule__LinearRegression__LinearRegressionAssignment");
			builder.put(grammarAccess.getVariablesAccess().getPredictionsAssignment_2(), "rule__Variables__PredictionsAssignment_2");
			builder.put(grammarAccess.getVariablesAccess().getTargetAssignment_5(), "rule__Variables__TargetAssignment_5");
			builder.put(grammarAccess.getDefaultTargetAccess().getDefaultAssignment(), "rule__DefaultTarget__DefaultAssignment");
			builder.put(grammarAccess.getNamedTargetAccess().getTargetAssignment(), "rule__NamedTarget__TargetAssignment");
			builder.put(grammarAccess.getAllVariablesAccess().getAllAssignment(), "rule__AllVariables__AllAssignment");
			builder.put(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_0(), "rule__PredictorVariables__VarsAssignment_0");
			builder.put(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_1_1(), "rule__PredictorVariables__VarsAssignment_1_1");
			builder.put(grammarAccess.getItemAccess().getColumnNameAssignment(), "rule__Item__ColumnNameAssignment");
			builder.put(grammarAccess.getEvaluationAccess().getStrategyEvaluationAssignment_1(), "rule__Evaluation__StrategyEvaluationAssignment_1");
			builder.put(grammarAccess.getSimpleSplitAccess().getValidationPercentAssignment_2(), "rule__SimpleSplit__ValidationPercentAssignment_2");
			builder.put(grammarAccess.getCrossValidationAccess().getNumberAssignment_2(), "rule__CrossValidation__NumberAssignment_2");
		}
	}
	
	@Inject
	private NameMappings nameMappings;

	@Inject
	private MMLGrammarAccess grammarAccess;

	@Override
	protected InternalMMLParser createParser() {
		InternalMMLParser result = new InternalMMLParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		return nameMappings.getRuleName(element);
	}

	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public MMLGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(MMLGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
	public NameMappings getNameMappings() {
		return nameMappings;
	}
	
	public void setNameMappings(NameMappings nameMappings) {
		this.nameMappings = nameMappings;
	}
}
