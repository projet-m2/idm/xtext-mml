package fr.istic.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import fr.istic.services.MMLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMMLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_INT", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "','", "';'", "'\\n'", "'Scikit-learn'", "'dataSet'", "'separator'", "'mlFramework'", "'mlAlgorithm'", "'variables'", "'prediction'", "'target'", "'+'", "'evaluation'", "'TrainingSet'", "'validationPercent'", "'CrossValidation'", "'partitionCount'", "'RegressionTree'", "'LinearRegression'", "'default'", "'all'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMMLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMMLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMMLParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMML.g"; }


    	private MMLGrammarAccess grammarAccess;

    	public void setGrammarAccess(MMLGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleMMLModel"
    // InternalMML.g:53:1: entryRuleMMLModel : ruleMMLModel EOF ;
    public final void entryRuleMMLModel() throws RecognitionException {
        try {
            // InternalMML.g:54:1: ( ruleMMLModel EOF )
            // InternalMML.g:55:1: ruleMMLModel EOF
            {
             before(grammarAccess.getMMLModelRule()); 
            pushFollow(FOLLOW_1);
            ruleMMLModel();

            state._fsp--;

             after(grammarAccess.getMMLModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMMLModel"


    // $ANTLR start "ruleMMLModel"
    // InternalMML.g:62:1: ruleMMLModel : ( ( rule__MMLModel__Group__0 ) ) ;
    public final void ruleMMLModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:66:2: ( ( ( rule__MMLModel__Group__0 ) ) )
            // InternalMML.g:67:2: ( ( rule__MMLModel__Group__0 ) )
            {
            // InternalMML.g:67:2: ( ( rule__MMLModel__Group__0 ) )
            // InternalMML.g:68:3: ( rule__MMLModel__Group__0 )
            {
             before(grammarAccess.getMMLModelAccess().getGroup()); 
            // InternalMML.g:69:3: ( rule__MMLModel__Group__0 )
            // InternalMML.g:69:4: rule__MMLModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMMLModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMMLModel"


    // $ANTLR start "ruleCOMMA"
    // InternalMML.g:79:1: ruleCOMMA : ( ',' ) ;
    public final void ruleCOMMA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:83:2: ( ( ',' ) )
            // InternalMML.g:84:2: ( ',' )
            {
            // InternalMML.g:84:2: ( ',' )
            // InternalMML.g:85:3: ','
            {
             before(grammarAccess.getCOMMAAccess().getCommaKeyword()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getCOMMAAccess().getCommaKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCOMMA"


    // $ANTLR start "ruleSEMICOLON"
    // InternalMML.g:96:1: ruleSEMICOLON : ( ';' ) ;
    public final void ruleSEMICOLON() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:100:2: ( ( ';' ) )
            // InternalMML.g:101:2: ( ';' )
            {
            // InternalMML.g:101:2: ( ';' )
            // InternalMML.g:102:3: ';'
            {
             before(grammarAccess.getSEMICOLONAccess().getSemicolonKeyword()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSEMICOLONAccess().getSemicolonKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSEMICOLON"


    // $ANTLR start "ruleNEWLINE"
    // InternalMML.g:113:1: ruleNEWLINE : ( '\\n' ) ;
    public final void ruleNEWLINE() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:117:2: ( ( '\\n' ) )
            // InternalMML.g:118:2: ( '\\n' )
            {
            // InternalMML.g:118:2: ( '\\n' )
            // InternalMML.g:119:3: '\\n'
            {
             before(grammarAccess.getNEWLINEAccess().getLineFeedKeyword()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getNEWLINEAccess().getLineFeedKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNEWLINE"


    // $ANTLR start "entryRuleDataInput"
    // InternalMML.g:129:1: entryRuleDataInput : ruleDataInput EOF ;
    public final void entryRuleDataInput() throws RecognitionException {
        try {
            // InternalMML.g:130:1: ( ruleDataInput EOF )
            // InternalMML.g:131:1: ruleDataInput EOF
            {
             before(grammarAccess.getDataInputRule()); 
            pushFollow(FOLLOW_1);
            ruleDataInput();

            state._fsp--;

             after(grammarAccess.getDataInputRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataInput"


    // $ANTLR start "ruleDataInput"
    // InternalMML.g:138:1: ruleDataInput : ( ( rule__DataInput__Group__0 ) ) ;
    public final void ruleDataInput() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:142:2: ( ( ( rule__DataInput__Group__0 ) ) )
            // InternalMML.g:143:2: ( ( rule__DataInput__Group__0 ) )
            {
            // InternalMML.g:143:2: ( ( rule__DataInput__Group__0 ) )
            // InternalMML.g:144:3: ( rule__DataInput__Group__0 )
            {
             before(grammarAccess.getDataInputAccess().getGroup()); 
            // InternalMML.g:145:3: ( rule__DataInput__Group__0 )
            // InternalMML.g:145:4: rule__DataInput__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataInput__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataInputAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataInput"


    // $ANTLR start "entryRuleCSVSeparator"
    // InternalMML.g:154:1: entryRuleCSVSeparator : ruleCSVSeparator EOF ;
    public final void entryRuleCSVSeparator() throws RecognitionException {
        try {
            // InternalMML.g:155:1: ( ruleCSVSeparator EOF )
            // InternalMML.g:156:1: ruleCSVSeparator EOF
            {
             before(grammarAccess.getCSVSeparatorRule()); 
            pushFollow(FOLLOW_1);
            ruleCSVSeparator();

            state._fsp--;

             after(grammarAccess.getCSVSeparatorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCSVSeparator"


    // $ANTLR start "ruleCSVSeparator"
    // InternalMML.g:163:1: ruleCSVSeparator : ( ( rule__CSVSeparator__Group__0 ) ) ;
    public final void ruleCSVSeparator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:167:2: ( ( ( rule__CSVSeparator__Group__0 ) ) )
            // InternalMML.g:168:2: ( ( rule__CSVSeparator__Group__0 ) )
            {
            // InternalMML.g:168:2: ( ( rule__CSVSeparator__Group__0 ) )
            // InternalMML.g:169:3: ( rule__CSVSeparator__Group__0 )
            {
             before(grammarAccess.getCSVSeparatorAccess().getGroup()); 
            // InternalMML.g:170:3: ( rule__CSVSeparator__Group__0 )
            // InternalMML.g:170:4: rule__CSVSeparator__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CSVSeparator__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCSVSeparatorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCSVSeparator"


    // $ANTLR start "entryRuleMLChoice"
    // InternalMML.g:179:1: entryRuleMLChoice : ruleMLChoice EOF ;
    public final void entryRuleMLChoice() throws RecognitionException {
        try {
            // InternalMML.g:180:1: ( ruleMLChoice EOF )
            // InternalMML.g:181:1: ruleMLChoice EOF
            {
             before(grammarAccess.getMLChoiceRule()); 
            pushFollow(FOLLOW_1);
            ruleMLChoice();

            state._fsp--;

             after(grammarAccess.getMLChoiceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMLChoice"


    // $ANTLR start "ruleMLChoice"
    // InternalMML.g:188:1: ruleMLChoice : ( ( rule__MLChoice__Group__0 ) ) ;
    public final void ruleMLChoice() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:192:2: ( ( ( rule__MLChoice__Group__0 ) ) )
            // InternalMML.g:193:2: ( ( rule__MLChoice__Group__0 ) )
            {
            // InternalMML.g:193:2: ( ( rule__MLChoice__Group__0 ) )
            // InternalMML.g:194:3: ( rule__MLChoice__Group__0 )
            {
             before(grammarAccess.getMLChoiceAccess().getGroup()); 
            // InternalMML.g:195:3: ( rule__MLChoice__Group__0 )
            // InternalMML.g:195:4: rule__MLChoice__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMLChoiceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMLChoice"


    // $ANTLR start "entryRuleMLAlgorithm"
    // InternalMML.g:204:1: entryRuleMLAlgorithm : ruleMLAlgorithm EOF ;
    public final void entryRuleMLAlgorithm() throws RecognitionException {
        try {
            // InternalMML.g:205:1: ( ruleMLAlgorithm EOF )
            // InternalMML.g:206:1: ruleMLAlgorithm EOF
            {
             before(grammarAccess.getMLAlgorithmRule()); 
            pushFollow(FOLLOW_1);
            ruleMLAlgorithm();

            state._fsp--;

             after(grammarAccess.getMLAlgorithmRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMLAlgorithm"


    // $ANTLR start "ruleMLAlgorithm"
    // InternalMML.g:213:1: ruleMLAlgorithm : ( ( rule__MLAlgorithm__Alternatives ) ) ;
    public final void ruleMLAlgorithm() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:217:2: ( ( ( rule__MLAlgorithm__Alternatives ) ) )
            // InternalMML.g:218:2: ( ( rule__MLAlgorithm__Alternatives ) )
            {
            // InternalMML.g:218:2: ( ( rule__MLAlgorithm__Alternatives ) )
            // InternalMML.g:219:3: ( rule__MLAlgorithm__Alternatives )
            {
             before(grammarAccess.getMLAlgorithmAccess().getAlternatives()); 
            // InternalMML.g:220:3: ( rule__MLAlgorithm__Alternatives )
            // InternalMML.g:220:4: rule__MLAlgorithm__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__MLAlgorithm__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMLAlgorithmAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMLAlgorithm"


    // $ANTLR start "entryRuleRegressionTree"
    // InternalMML.g:229:1: entryRuleRegressionTree : ruleRegressionTree EOF ;
    public final void entryRuleRegressionTree() throws RecognitionException {
        try {
            // InternalMML.g:230:1: ( ruleRegressionTree EOF )
            // InternalMML.g:231:1: ruleRegressionTree EOF
            {
             before(grammarAccess.getRegressionTreeRule()); 
            pushFollow(FOLLOW_1);
            ruleRegressionTree();

            state._fsp--;

             after(grammarAccess.getRegressionTreeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRegressionTree"


    // $ANTLR start "ruleRegressionTree"
    // InternalMML.g:238:1: ruleRegressionTree : ( ( rule__RegressionTree__RegressionTreeAssignment ) ) ;
    public final void ruleRegressionTree() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:242:2: ( ( ( rule__RegressionTree__RegressionTreeAssignment ) ) )
            // InternalMML.g:243:2: ( ( rule__RegressionTree__RegressionTreeAssignment ) )
            {
            // InternalMML.g:243:2: ( ( rule__RegressionTree__RegressionTreeAssignment ) )
            // InternalMML.g:244:3: ( rule__RegressionTree__RegressionTreeAssignment )
            {
             before(grammarAccess.getRegressionTreeAccess().getRegressionTreeAssignment()); 
            // InternalMML.g:245:3: ( rule__RegressionTree__RegressionTreeAssignment )
            // InternalMML.g:245:4: rule__RegressionTree__RegressionTreeAssignment
            {
            pushFollow(FOLLOW_2);
            rule__RegressionTree__RegressionTreeAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRegressionTreeAccess().getRegressionTreeAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRegressionTree"


    // $ANTLR start "entryRuleLinearRegression"
    // InternalMML.g:254:1: entryRuleLinearRegression : ruleLinearRegression EOF ;
    public final void entryRuleLinearRegression() throws RecognitionException {
        try {
            // InternalMML.g:255:1: ( ruleLinearRegression EOF )
            // InternalMML.g:256:1: ruleLinearRegression EOF
            {
             before(grammarAccess.getLinearRegressionRule()); 
            pushFollow(FOLLOW_1);
            ruleLinearRegression();

            state._fsp--;

             after(grammarAccess.getLinearRegressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLinearRegression"


    // $ANTLR start "ruleLinearRegression"
    // InternalMML.g:263:1: ruleLinearRegression : ( ( rule__LinearRegression__LinearRegressionAssignment ) ) ;
    public final void ruleLinearRegression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:267:2: ( ( ( rule__LinearRegression__LinearRegressionAssignment ) ) )
            // InternalMML.g:268:2: ( ( rule__LinearRegression__LinearRegressionAssignment ) )
            {
            // InternalMML.g:268:2: ( ( rule__LinearRegression__LinearRegressionAssignment ) )
            // InternalMML.g:269:3: ( rule__LinearRegression__LinearRegressionAssignment )
            {
             before(grammarAccess.getLinearRegressionAccess().getLinearRegressionAssignment()); 
            // InternalMML.g:270:3: ( rule__LinearRegression__LinearRegressionAssignment )
            // InternalMML.g:270:4: rule__LinearRegression__LinearRegressionAssignment
            {
            pushFollow(FOLLOW_2);
            rule__LinearRegression__LinearRegressionAssignment();

            state._fsp--;


            }

             after(grammarAccess.getLinearRegressionAccess().getLinearRegressionAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLinearRegression"


    // $ANTLR start "entryRuleVariables"
    // InternalMML.g:279:1: entryRuleVariables : ruleVariables EOF ;
    public final void entryRuleVariables() throws RecognitionException {
        try {
            // InternalMML.g:280:1: ( ruleVariables EOF )
            // InternalMML.g:281:1: ruleVariables EOF
            {
             before(grammarAccess.getVariablesRule()); 
            pushFollow(FOLLOW_1);
            ruleVariables();

            state._fsp--;

             after(grammarAccess.getVariablesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariables"


    // $ANTLR start "ruleVariables"
    // InternalMML.g:288:1: ruleVariables : ( ( rule__Variables__Group__0 ) ) ;
    public final void ruleVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:292:2: ( ( ( rule__Variables__Group__0 ) ) )
            // InternalMML.g:293:2: ( ( rule__Variables__Group__0 ) )
            {
            // InternalMML.g:293:2: ( ( rule__Variables__Group__0 ) )
            // InternalMML.g:294:3: ( rule__Variables__Group__0 )
            {
             before(grammarAccess.getVariablesAccess().getGroup()); 
            // InternalMML.g:295:3: ( rule__Variables__Group__0 )
            // InternalMML.g:295:4: rule__Variables__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variables__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariablesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariables"


    // $ANTLR start "entryRuleTarget"
    // InternalMML.g:304:1: entryRuleTarget : ruleTarget EOF ;
    public final void entryRuleTarget() throws RecognitionException {
        try {
            // InternalMML.g:305:1: ( ruleTarget EOF )
            // InternalMML.g:306:1: ruleTarget EOF
            {
             before(grammarAccess.getTargetRule()); 
            pushFollow(FOLLOW_1);
            ruleTarget();

            state._fsp--;

             after(grammarAccess.getTargetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTarget"


    // $ANTLR start "ruleTarget"
    // InternalMML.g:313:1: ruleTarget : ( ( rule__Target__Alternatives ) ) ;
    public final void ruleTarget() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:317:2: ( ( ( rule__Target__Alternatives ) ) )
            // InternalMML.g:318:2: ( ( rule__Target__Alternatives ) )
            {
            // InternalMML.g:318:2: ( ( rule__Target__Alternatives ) )
            // InternalMML.g:319:3: ( rule__Target__Alternatives )
            {
             before(grammarAccess.getTargetAccess().getAlternatives()); 
            // InternalMML.g:320:3: ( rule__Target__Alternatives )
            // InternalMML.g:320:4: rule__Target__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Target__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTargetAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTarget"


    // $ANTLR start "entryRuleDefaultTarget"
    // InternalMML.g:329:1: entryRuleDefaultTarget : ruleDefaultTarget EOF ;
    public final void entryRuleDefaultTarget() throws RecognitionException {
        try {
            // InternalMML.g:330:1: ( ruleDefaultTarget EOF )
            // InternalMML.g:331:1: ruleDefaultTarget EOF
            {
             before(grammarAccess.getDefaultTargetRule()); 
            pushFollow(FOLLOW_1);
            ruleDefaultTarget();

            state._fsp--;

             after(grammarAccess.getDefaultTargetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDefaultTarget"


    // $ANTLR start "ruleDefaultTarget"
    // InternalMML.g:338:1: ruleDefaultTarget : ( ( rule__DefaultTarget__DefaultAssignment ) ) ;
    public final void ruleDefaultTarget() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:342:2: ( ( ( rule__DefaultTarget__DefaultAssignment ) ) )
            // InternalMML.g:343:2: ( ( rule__DefaultTarget__DefaultAssignment ) )
            {
            // InternalMML.g:343:2: ( ( rule__DefaultTarget__DefaultAssignment ) )
            // InternalMML.g:344:3: ( rule__DefaultTarget__DefaultAssignment )
            {
             before(grammarAccess.getDefaultTargetAccess().getDefaultAssignment()); 
            // InternalMML.g:345:3: ( rule__DefaultTarget__DefaultAssignment )
            // InternalMML.g:345:4: rule__DefaultTarget__DefaultAssignment
            {
            pushFollow(FOLLOW_2);
            rule__DefaultTarget__DefaultAssignment();

            state._fsp--;


            }

             after(grammarAccess.getDefaultTargetAccess().getDefaultAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDefaultTarget"


    // $ANTLR start "entryRuleNamedTarget"
    // InternalMML.g:354:1: entryRuleNamedTarget : ruleNamedTarget EOF ;
    public final void entryRuleNamedTarget() throws RecognitionException {
        try {
            // InternalMML.g:355:1: ( ruleNamedTarget EOF )
            // InternalMML.g:356:1: ruleNamedTarget EOF
            {
             before(grammarAccess.getNamedTargetRule()); 
            pushFollow(FOLLOW_1);
            ruleNamedTarget();

            state._fsp--;

             after(grammarAccess.getNamedTargetRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNamedTarget"


    // $ANTLR start "ruleNamedTarget"
    // InternalMML.g:363:1: ruleNamedTarget : ( ( rule__NamedTarget__TargetAssignment ) ) ;
    public final void ruleNamedTarget() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:367:2: ( ( ( rule__NamedTarget__TargetAssignment ) ) )
            // InternalMML.g:368:2: ( ( rule__NamedTarget__TargetAssignment ) )
            {
            // InternalMML.g:368:2: ( ( rule__NamedTarget__TargetAssignment ) )
            // InternalMML.g:369:3: ( rule__NamedTarget__TargetAssignment )
            {
             before(grammarAccess.getNamedTargetAccess().getTargetAssignment()); 
            // InternalMML.g:370:3: ( rule__NamedTarget__TargetAssignment )
            // InternalMML.g:370:4: rule__NamedTarget__TargetAssignment
            {
            pushFollow(FOLLOW_2);
            rule__NamedTarget__TargetAssignment();

            state._fsp--;


            }

             after(grammarAccess.getNamedTargetAccess().getTargetAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNamedTarget"


    // $ANTLR start "entryRulePredictions"
    // InternalMML.g:379:1: entryRulePredictions : rulePredictions EOF ;
    public final void entryRulePredictions() throws RecognitionException {
        try {
            // InternalMML.g:380:1: ( rulePredictions EOF )
            // InternalMML.g:381:1: rulePredictions EOF
            {
             before(grammarAccess.getPredictionsRule()); 
            pushFollow(FOLLOW_1);
            rulePredictions();

            state._fsp--;

             after(grammarAccess.getPredictionsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredictions"


    // $ANTLR start "rulePredictions"
    // InternalMML.g:388:1: rulePredictions : ( ( rule__Predictions__Alternatives ) ) ;
    public final void rulePredictions() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:392:2: ( ( ( rule__Predictions__Alternatives ) ) )
            // InternalMML.g:393:2: ( ( rule__Predictions__Alternatives ) )
            {
            // InternalMML.g:393:2: ( ( rule__Predictions__Alternatives ) )
            // InternalMML.g:394:3: ( rule__Predictions__Alternatives )
            {
             before(grammarAccess.getPredictionsAccess().getAlternatives()); 
            // InternalMML.g:395:3: ( rule__Predictions__Alternatives )
            // InternalMML.g:395:4: rule__Predictions__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Predictions__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPredictionsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredictions"


    // $ANTLR start "entryRuleAllVariables"
    // InternalMML.g:404:1: entryRuleAllVariables : ruleAllVariables EOF ;
    public final void entryRuleAllVariables() throws RecognitionException {
        try {
            // InternalMML.g:405:1: ( ruleAllVariables EOF )
            // InternalMML.g:406:1: ruleAllVariables EOF
            {
             before(grammarAccess.getAllVariablesRule()); 
            pushFollow(FOLLOW_1);
            ruleAllVariables();

            state._fsp--;

             after(grammarAccess.getAllVariablesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAllVariables"


    // $ANTLR start "ruleAllVariables"
    // InternalMML.g:413:1: ruleAllVariables : ( ( rule__AllVariables__AllAssignment ) ) ;
    public final void ruleAllVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:417:2: ( ( ( rule__AllVariables__AllAssignment ) ) )
            // InternalMML.g:418:2: ( ( rule__AllVariables__AllAssignment ) )
            {
            // InternalMML.g:418:2: ( ( rule__AllVariables__AllAssignment ) )
            // InternalMML.g:419:3: ( rule__AllVariables__AllAssignment )
            {
             before(grammarAccess.getAllVariablesAccess().getAllAssignment()); 
            // InternalMML.g:420:3: ( rule__AllVariables__AllAssignment )
            // InternalMML.g:420:4: rule__AllVariables__AllAssignment
            {
            pushFollow(FOLLOW_2);
            rule__AllVariables__AllAssignment();

            state._fsp--;


            }

             after(grammarAccess.getAllVariablesAccess().getAllAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAllVariables"


    // $ANTLR start "entryRulePredictorVariables"
    // InternalMML.g:429:1: entryRulePredictorVariables : rulePredictorVariables EOF ;
    public final void entryRulePredictorVariables() throws RecognitionException {
        try {
            // InternalMML.g:430:1: ( rulePredictorVariables EOF )
            // InternalMML.g:431:1: rulePredictorVariables EOF
            {
             before(grammarAccess.getPredictorVariablesRule()); 
            pushFollow(FOLLOW_1);
            rulePredictorVariables();

            state._fsp--;

             after(grammarAccess.getPredictorVariablesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredictorVariables"


    // $ANTLR start "rulePredictorVariables"
    // InternalMML.g:438:1: rulePredictorVariables : ( ( rule__PredictorVariables__Group__0 ) ) ;
    public final void rulePredictorVariables() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:442:2: ( ( ( rule__PredictorVariables__Group__0 ) ) )
            // InternalMML.g:443:2: ( ( rule__PredictorVariables__Group__0 ) )
            {
            // InternalMML.g:443:2: ( ( rule__PredictorVariables__Group__0 ) )
            // InternalMML.g:444:3: ( rule__PredictorVariables__Group__0 )
            {
             before(grammarAccess.getPredictorVariablesAccess().getGroup()); 
            // InternalMML.g:445:3: ( rule__PredictorVariables__Group__0 )
            // InternalMML.g:445:4: rule__PredictorVariables__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PredictorVariables__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPredictorVariablesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredictorVariables"


    // $ANTLR start "entryRuleItem"
    // InternalMML.g:454:1: entryRuleItem : ruleItem EOF ;
    public final void entryRuleItem() throws RecognitionException {
        try {
            // InternalMML.g:455:1: ( ruleItem EOF )
            // InternalMML.g:456:1: ruleItem EOF
            {
             before(grammarAccess.getItemRule()); 
            pushFollow(FOLLOW_1);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getItemRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleItem"


    // $ANTLR start "ruleItem"
    // InternalMML.g:463:1: ruleItem : ( ( rule__Item__ColumnNameAssignment ) ) ;
    public final void ruleItem() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:467:2: ( ( ( rule__Item__ColumnNameAssignment ) ) )
            // InternalMML.g:468:2: ( ( rule__Item__ColumnNameAssignment ) )
            {
            // InternalMML.g:468:2: ( ( rule__Item__ColumnNameAssignment ) )
            // InternalMML.g:469:3: ( rule__Item__ColumnNameAssignment )
            {
             before(grammarAccess.getItemAccess().getColumnNameAssignment()); 
            // InternalMML.g:470:3: ( rule__Item__ColumnNameAssignment )
            // InternalMML.g:470:4: rule__Item__ColumnNameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Item__ColumnNameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getItemAccess().getColumnNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleItem"


    // $ANTLR start "entryRuleEvaluation"
    // InternalMML.g:479:1: entryRuleEvaluation : ruleEvaluation EOF ;
    public final void entryRuleEvaluation() throws RecognitionException {
        try {
            // InternalMML.g:480:1: ( ruleEvaluation EOF )
            // InternalMML.g:481:1: ruleEvaluation EOF
            {
             before(grammarAccess.getEvaluationRule()); 
            pushFollow(FOLLOW_1);
            ruleEvaluation();

            state._fsp--;

             after(grammarAccess.getEvaluationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEvaluation"


    // $ANTLR start "ruleEvaluation"
    // InternalMML.g:488:1: ruleEvaluation : ( ( rule__Evaluation__Group__0 ) ) ;
    public final void ruleEvaluation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:492:2: ( ( ( rule__Evaluation__Group__0 ) ) )
            // InternalMML.g:493:2: ( ( rule__Evaluation__Group__0 ) )
            {
            // InternalMML.g:493:2: ( ( rule__Evaluation__Group__0 ) )
            // InternalMML.g:494:3: ( rule__Evaluation__Group__0 )
            {
             before(grammarAccess.getEvaluationAccess().getGroup()); 
            // InternalMML.g:495:3: ( rule__Evaluation__Group__0 )
            // InternalMML.g:495:4: rule__Evaluation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Evaluation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEvaluationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEvaluation"


    // $ANTLR start "entryRuleStrategyEvaluation"
    // InternalMML.g:504:1: entryRuleStrategyEvaluation : ruleStrategyEvaluation EOF ;
    public final void entryRuleStrategyEvaluation() throws RecognitionException {
        try {
            // InternalMML.g:505:1: ( ruleStrategyEvaluation EOF )
            // InternalMML.g:506:1: ruleStrategyEvaluation EOF
            {
             before(grammarAccess.getStrategyEvaluationRule()); 
            pushFollow(FOLLOW_1);
            ruleStrategyEvaluation();

            state._fsp--;

             after(grammarAccess.getStrategyEvaluationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStrategyEvaluation"


    // $ANTLR start "ruleStrategyEvaluation"
    // InternalMML.g:513:1: ruleStrategyEvaluation : ( ( rule__StrategyEvaluation__Alternatives ) ) ;
    public final void ruleStrategyEvaluation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:517:2: ( ( ( rule__StrategyEvaluation__Alternatives ) ) )
            // InternalMML.g:518:2: ( ( rule__StrategyEvaluation__Alternatives ) )
            {
            // InternalMML.g:518:2: ( ( rule__StrategyEvaluation__Alternatives ) )
            // InternalMML.g:519:3: ( rule__StrategyEvaluation__Alternatives )
            {
             before(grammarAccess.getStrategyEvaluationAccess().getAlternatives()); 
            // InternalMML.g:520:3: ( rule__StrategyEvaluation__Alternatives )
            // InternalMML.g:520:4: rule__StrategyEvaluation__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__StrategyEvaluation__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStrategyEvaluationAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStrategyEvaluation"


    // $ANTLR start "entryRuleSimpleSplit"
    // InternalMML.g:529:1: entryRuleSimpleSplit : ruleSimpleSplit EOF ;
    public final void entryRuleSimpleSplit() throws RecognitionException {
        try {
            // InternalMML.g:530:1: ( ruleSimpleSplit EOF )
            // InternalMML.g:531:1: ruleSimpleSplit EOF
            {
             before(grammarAccess.getSimpleSplitRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleSplit();

            state._fsp--;

             after(grammarAccess.getSimpleSplitRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleSplit"


    // $ANTLR start "ruleSimpleSplit"
    // InternalMML.g:538:1: ruleSimpleSplit : ( ( rule__SimpleSplit__Group__0 ) ) ;
    public final void ruleSimpleSplit() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:542:2: ( ( ( rule__SimpleSplit__Group__0 ) ) )
            // InternalMML.g:543:2: ( ( rule__SimpleSplit__Group__0 ) )
            {
            // InternalMML.g:543:2: ( ( rule__SimpleSplit__Group__0 ) )
            // InternalMML.g:544:3: ( rule__SimpleSplit__Group__0 )
            {
             before(grammarAccess.getSimpleSplitAccess().getGroup()); 
            // InternalMML.g:545:3: ( rule__SimpleSplit__Group__0 )
            // InternalMML.g:545:4: rule__SimpleSplit__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSplit__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSimpleSplitAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleSplit"


    // $ANTLR start "entryRuleCrossValidation"
    // InternalMML.g:554:1: entryRuleCrossValidation : ruleCrossValidation EOF ;
    public final void entryRuleCrossValidation() throws RecognitionException {
        try {
            // InternalMML.g:555:1: ( ruleCrossValidation EOF )
            // InternalMML.g:556:1: ruleCrossValidation EOF
            {
             before(grammarAccess.getCrossValidationRule()); 
            pushFollow(FOLLOW_1);
            ruleCrossValidation();

            state._fsp--;

             after(grammarAccess.getCrossValidationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCrossValidation"


    // $ANTLR start "ruleCrossValidation"
    // InternalMML.g:563:1: ruleCrossValidation : ( ( rule__CrossValidation__Group__0 ) ) ;
    public final void ruleCrossValidation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:567:2: ( ( ( rule__CrossValidation__Group__0 ) ) )
            // InternalMML.g:568:2: ( ( rule__CrossValidation__Group__0 ) )
            {
            // InternalMML.g:568:2: ( ( rule__CrossValidation__Group__0 ) )
            // InternalMML.g:569:3: ( rule__CrossValidation__Group__0 )
            {
             before(grammarAccess.getCrossValidationAccess().getGroup()); 
            // InternalMML.g:570:3: ( rule__CrossValidation__Group__0 )
            // InternalMML.g:570:4: rule__CrossValidation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CrossValidation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCrossValidationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCrossValidation"


    // $ANTLR start "ruleMLFramework"
    // InternalMML.g:579:1: ruleMLFramework : ( ( 'Scikit-learn' ) ) ;
    public final void ruleMLFramework() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:583:1: ( ( ( 'Scikit-learn' ) ) )
            // InternalMML.g:584:2: ( ( 'Scikit-learn' ) )
            {
            // InternalMML.g:584:2: ( ( 'Scikit-learn' ) )
            // InternalMML.g:585:3: ( 'Scikit-learn' )
            {
             before(grammarAccess.getMLFrameworkAccess().getSCIKITLEARNEnumLiteralDeclaration()); 
            // InternalMML.g:586:3: ( 'Scikit-learn' )
            // InternalMML.g:586:4: 'Scikit-learn'
            {
            match(input,14,FOLLOW_2); 

            }

             after(grammarAccess.getMLFrameworkAccess().getSCIKITLEARNEnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMLFramework"


    // $ANTLR start "rule__MLAlgorithm__Alternatives"
    // InternalMML.g:594:1: rule__MLAlgorithm__Alternatives : ( ( ruleRegressionTree ) | ( ruleLinearRegression ) );
    public final void rule__MLAlgorithm__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:598:1: ( ( ruleRegressionTree ) | ( ruleLinearRegression ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==28) ) {
                alt1=1;
            }
            else if ( (LA1_0==29) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMML.g:599:2: ( ruleRegressionTree )
                    {
                    // InternalMML.g:599:2: ( ruleRegressionTree )
                    // InternalMML.g:600:3: ruleRegressionTree
                    {
                     before(grammarAccess.getMLAlgorithmAccess().getRegressionTreeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleRegressionTree();

                    state._fsp--;

                     after(grammarAccess.getMLAlgorithmAccess().getRegressionTreeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMML.g:605:2: ( ruleLinearRegression )
                    {
                    // InternalMML.g:605:2: ( ruleLinearRegression )
                    // InternalMML.g:606:3: ruleLinearRegression
                    {
                     before(grammarAccess.getMLAlgorithmAccess().getLinearRegressionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleLinearRegression();

                    state._fsp--;

                     after(grammarAccess.getMLAlgorithmAccess().getLinearRegressionParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLAlgorithm__Alternatives"


    // $ANTLR start "rule__Target__Alternatives"
    // InternalMML.g:615:1: rule__Target__Alternatives : ( ( ruleDefaultTarget ) | ( ruleNamedTarget ) );
    public final void rule__Target__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:619:1: ( ( ruleDefaultTarget ) | ( ruleNamedTarget ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==30) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_STRING) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalMML.g:620:2: ( ruleDefaultTarget )
                    {
                    // InternalMML.g:620:2: ( ruleDefaultTarget )
                    // InternalMML.g:621:3: ruleDefaultTarget
                    {
                     before(grammarAccess.getTargetAccess().getDefaultTargetParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDefaultTarget();

                    state._fsp--;

                     after(grammarAccess.getTargetAccess().getDefaultTargetParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMML.g:626:2: ( ruleNamedTarget )
                    {
                    // InternalMML.g:626:2: ( ruleNamedTarget )
                    // InternalMML.g:627:3: ruleNamedTarget
                    {
                     before(grammarAccess.getTargetAccess().getNamedTargetParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleNamedTarget();

                    state._fsp--;

                     after(grammarAccess.getTargetAccess().getNamedTargetParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Target__Alternatives"


    // $ANTLR start "rule__Predictions__Alternatives"
    // InternalMML.g:636:1: rule__Predictions__Alternatives : ( ( ruleAllVariables ) | ( rulePredictorVariables ) );
    public final void rule__Predictions__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:640:1: ( ( ruleAllVariables ) | ( rulePredictorVariables ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==31) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_STRING) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMML.g:641:2: ( ruleAllVariables )
                    {
                    // InternalMML.g:641:2: ( ruleAllVariables )
                    // InternalMML.g:642:3: ruleAllVariables
                    {
                     before(grammarAccess.getPredictionsAccess().getAllVariablesParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAllVariables();

                    state._fsp--;

                     after(grammarAccess.getPredictionsAccess().getAllVariablesParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMML.g:647:2: ( rulePredictorVariables )
                    {
                    // InternalMML.g:647:2: ( rulePredictorVariables )
                    // InternalMML.g:648:3: rulePredictorVariables
                    {
                     before(grammarAccess.getPredictionsAccess().getPredictorVariablesParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    rulePredictorVariables();

                    state._fsp--;

                     after(grammarAccess.getPredictionsAccess().getPredictorVariablesParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predictions__Alternatives"


    // $ANTLR start "rule__StrategyEvaluation__Alternatives"
    // InternalMML.g:657:1: rule__StrategyEvaluation__Alternatives : ( ( ruleSimpleSplit ) | ( ruleCrossValidation ) );
    public final void rule__StrategyEvaluation__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:661:1: ( ( ruleSimpleSplit ) | ( ruleCrossValidation ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==24) ) {
                alt4=1;
            }
            else if ( (LA4_0==26) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMML.g:662:2: ( ruleSimpleSplit )
                    {
                    // InternalMML.g:662:2: ( ruleSimpleSplit )
                    // InternalMML.g:663:3: ruleSimpleSplit
                    {
                     before(grammarAccess.getStrategyEvaluationAccess().getSimpleSplitParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSimpleSplit();

                    state._fsp--;

                     after(grammarAccess.getStrategyEvaluationAccess().getSimpleSplitParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMML.g:668:2: ( ruleCrossValidation )
                    {
                    // InternalMML.g:668:2: ( ruleCrossValidation )
                    // InternalMML.g:669:3: ruleCrossValidation
                    {
                     before(grammarAccess.getStrategyEvaluationAccess().getCrossValidationParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCrossValidation();

                    state._fsp--;

                     after(grammarAccess.getStrategyEvaluationAccess().getCrossValidationParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StrategyEvaluation__Alternatives"


    // $ANTLR start "rule__MMLModel__Group__0"
    // InternalMML.g:678:1: rule__MMLModel__Group__0 : rule__MMLModel__Group__0__Impl rule__MMLModel__Group__1 ;
    public final void rule__MMLModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:682:1: ( rule__MMLModel__Group__0__Impl rule__MMLModel__Group__1 )
            // InternalMML.g:683:2: rule__MMLModel__Group__0__Impl rule__MMLModel__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__MMLModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MMLModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__0"


    // $ANTLR start "rule__MMLModel__Group__0__Impl"
    // InternalMML.g:690:1: rule__MMLModel__Group__0__Impl : ( ( rule__MMLModel__InputAssignment_0 ) ) ;
    public final void rule__MMLModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:694:1: ( ( ( rule__MMLModel__InputAssignment_0 ) ) )
            // InternalMML.g:695:1: ( ( rule__MMLModel__InputAssignment_0 ) )
            {
            // InternalMML.g:695:1: ( ( rule__MMLModel__InputAssignment_0 ) )
            // InternalMML.g:696:2: ( rule__MMLModel__InputAssignment_0 )
            {
             before(grammarAccess.getMMLModelAccess().getInputAssignment_0()); 
            // InternalMML.g:697:2: ( rule__MMLModel__InputAssignment_0 )
            // InternalMML.g:697:3: rule__MMLModel__InputAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__InputAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMMLModelAccess().getInputAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__0__Impl"


    // $ANTLR start "rule__MMLModel__Group__1"
    // InternalMML.g:705:1: rule__MMLModel__Group__1 : rule__MMLModel__Group__1__Impl rule__MMLModel__Group__2 ;
    public final void rule__MMLModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:709:1: ( rule__MMLModel__Group__1__Impl rule__MMLModel__Group__2 )
            // InternalMML.g:710:2: rule__MMLModel__Group__1__Impl rule__MMLModel__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__MMLModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MMLModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__1"


    // $ANTLR start "rule__MMLModel__Group__1__Impl"
    // InternalMML.g:717:1: rule__MMLModel__Group__1__Impl : ( ( rule__MMLModel__MlChoiceAssignment_1 ) ) ;
    public final void rule__MMLModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:721:1: ( ( ( rule__MMLModel__MlChoiceAssignment_1 ) ) )
            // InternalMML.g:722:1: ( ( rule__MMLModel__MlChoiceAssignment_1 ) )
            {
            // InternalMML.g:722:1: ( ( rule__MMLModel__MlChoiceAssignment_1 ) )
            // InternalMML.g:723:2: ( rule__MMLModel__MlChoiceAssignment_1 )
            {
             before(grammarAccess.getMMLModelAccess().getMlChoiceAssignment_1()); 
            // InternalMML.g:724:2: ( rule__MMLModel__MlChoiceAssignment_1 )
            // InternalMML.g:724:3: rule__MMLModel__MlChoiceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__MlChoiceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMMLModelAccess().getMlChoiceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__1__Impl"


    // $ANTLR start "rule__MMLModel__Group__2"
    // InternalMML.g:732:1: rule__MMLModel__Group__2 : rule__MMLModel__Group__2__Impl rule__MMLModel__Group__3 ;
    public final void rule__MMLModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:736:1: ( rule__MMLModel__Group__2__Impl rule__MMLModel__Group__3 )
            // InternalMML.g:737:2: rule__MMLModel__Group__2__Impl rule__MMLModel__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__MMLModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MMLModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__2"


    // $ANTLR start "rule__MMLModel__Group__2__Impl"
    // InternalMML.g:744:1: rule__MMLModel__Group__2__Impl : ( ( rule__MMLModel__VariablesAssignment_2 ) ) ;
    public final void rule__MMLModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:748:1: ( ( ( rule__MMLModel__VariablesAssignment_2 ) ) )
            // InternalMML.g:749:1: ( ( rule__MMLModel__VariablesAssignment_2 ) )
            {
            // InternalMML.g:749:1: ( ( rule__MMLModel__VariablesAssignment_2 ) )
            // InternalMML.g:750:2: ( rule__MMLModel__VariablesAssignment_2 )
            {
             before(grammarAccess.getMMLModelAccess().getVariablesAssignment_2()); 
            // InternalMML.g:751:2: ( rule__MMLModel__VariablesAssignment_2 )
            // InternalMML.g:751:3: rule__MMLModel__VariablesAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__VariablesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMMLModelAccess().getVariablesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__2__Impl"


    // $ANTLR start "rule__MMLModel__Group__3"
    // InternalMML.g:759:1: rule__MMLModel__Group__3 : rule__MMLModel__Group__3__Impl ;
    public final void rule__MMLModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:763:1: ( rule__MMLModel__Group__3__Impl )
            // InternalMML.g:764:2: rule__MMLModel__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__3"


    // $ANTLR start "rule__MMLModel__Group__3__Impl"
    // InternalMML.g:770:1: rule__MMLModel__Group__3__Impl : ( ( rule__MMLModel__EvaluationAssignment_3 ) ) ;
    public final void rule__MMLModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:774:1: ( ( ( rule__MMLModel__EvaluationAssignment_3 ) ) )
            // InternalMML.g:775:1: ( ( rule__MMLModel__EvaluationAssignment_3 ) )
            {
            // InternalMML.g:775:1: ( ( rule__MMLModel__EvaluationAssignment_3 ) )
            // InternalMML.g:776:2: ( rule__MMLModel__EvaluationAssignment_3 )
            {
             before(grammarAccess.getMMLModelAccess().getEvaluationAssignment_3()); 
            // InternalMML.g:777:2: ( rule__MMLModel__EvaluationAssignment_3 )
            // InternalMML.g:777:3: rule__MMLModel__EvaluationAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__MMLModel__EvaluationAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getMMLModelAccess().getEvaluationAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__Group__3__Impl"


    // $ANTLR start "rule__DataInput__Group__0"
    // InternalMML.g:786:1: rule__DataInput__Group__0 : rule__DataInput__Group__0__Impl rule__DataInput__Group__1 ;
    public final void rule__DataInput__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:790:1: ( rule__DataInput__Group__0__Impl rule__DataInput__Group__1 )
            // InternalMML.g:791:2: rule__DataInput__Group__0__Impl rule__DataInput__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__DataInput__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataInput__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__0"


    // $ANTLR start "rule__DataInput__Group__0__Impl"
    // InternalMML.g:798:1: rule__DataInput__Group__0__Impl : ( 'dataSet' ) ;
    public final void rule__DataInput__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:802:1: ( ( 'dataSet' ) )
            // InternalMML.g:803:1: ( 'dataSet' )
            {
            // InternalMML.g:803:1: ( 'dataSet' )
            // InternalMML.g:804:2: 'dataSet'
            {
             before(grammarAccess.getDataInputAccess().getDataSetKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getDataInputAccess().getDataSetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__0__Impl"


    // $ANTLR start "rule__DataInput__Group__1"
    // InternalMML.g:813:1: rule__DataInput__Group__1 : rule__DataInput__Group__1__Impl rule__DataInput__Group__2 ;
    public final void rule__DataInput__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:817:1: ( rule__DataInput__Group__1__Impl rule__DataInput__Group__2 )
            // InternalMML.g:818:2: rule__DataInput__Group__1__Impl rule__DataInput__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__DataInput__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataInput__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__1"


    // $ANTLR start "rule__DataInput__Group__1__Impl"
    // InternalMML.g:825:1: rule__DataInput__Group__1__Impl : ( ( rule__DataInput__FileAssignment_1 ) ) ;
    public final void rule__DataInput__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:829:1: ( ( ( rule__DataInput__FileAssignment_1 ) ) )
            // InternalMML.g:830:1: ( ( rule__DataInput__FileAssignment_1 ) )
            {
            // InternalMML.g:830:1: ( ( rule__DataInput__FileAssignment_1 ) )
            // InternalMML.g:831:2: ( rule__DataInput__FileAssignment_1 )
            {
             before(grammarAccess.getDataInputAccess().getFileAssignment_1()); 
            // InternalMML.g:832:2: ( rule__DataInput__FileAssignment_1 )
            // InternalMML.g:832:3: rule__DataInput__FileAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataInput__FileAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataInputAccess().getFileAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__1__Impl"


    // $ANTLR start "rule__DataInput__Group__2"
    // InternalMML.g:840:1: rule__DataInput__Group__2 : rule__DataInput__Group__2__Impl rule__DataInput__Group__3 ;
    public final void rule__DataInput__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:844:1: ( rule__DataInput__Group__2__Impl rule__DataInput__Group__3 )
            // InternalMML.g:845:2: rule__DataInput__Group__2__Impl rule__DataInput__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__DataInput__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataInput__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__2"


    // $ANTLR start "rule__DataInput__Group__2__Impl"
    // InternalMML.g:852:1: rule__DataInput__Group__2__Impl : ( ( rule__DataInput__CsvSeparatorAssignment_2 )? ) ;
    public final void rule__DataInput__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:856:1: ( ( ( rule__DataInput__CsvSeparatorAssignment_2 )? ) )
            // InternalMML.g:857:1: ( ( rule__DataInput__CsvSeparatorAssignment_2 )? )
            {
            // InternalMML.g:857:1: ( ( rule__DataInput__CsvSeparatorAssignment_2 )? )
            // InternalMML.g:858:2: ( rule__DataInput__CsvSeparatorAssignment_2 )?
            {
             before(grammarAccess.getDataInputAccess().getCsvSeparatorAssignment_2()); 
            // InternalMML.g:859:2: ( rule__DataInput__CsvSeparatorAssignment_2 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalMML.g:859:3: rule__DataInput__CsvSeparatorAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataInput__CsvSeparatorAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataInputAccess().getCsvSeparatorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__2__Impl"


    // $ANTLR start "rule__DataInput__Group__3"
    // InternalMML.g:867:1: rule__DataInput__Group__3 : rule__DataInput__Group__3__Impl rule__DataInput__Group__4 ;
    public final void rule__DataInput__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:871:1: ( rule__DataInput__Group__3__Impl rule__DataInput__Group__4 )
            // InternalMML.g:872:2: rule__DataInput__Group__3__Impl rule__DataInput__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__DataInput__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataInput__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__3"


    // $ANTLR start "rule__DataInput__Group__3__Impl"
    // InternalMML.g:879:1: rule__DataInput__Group__3__Impl : ( ruleSEMICOLON ) ;
    public final void rule__DataInput__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:883:1: ( ( ruleSEMICOLON ) )
            // InternalMML.g:884:1: ( ruleSEMICOLON )
            {
            // InternalMML.g:884:1: ( ruleSEMICOLON )
            // InternalMML.g:885:2: ruleSEMICOLON
            {
             before(grammarAccess.getDataInputAccess().getSEMICOLONParserRuleCall_3()); 
            pushFollow(FOLLOW_2);
            ruleSEMICOLON();

            state._fsp--;

             after(grammarAccess.getDataInputAccess().getSEMICOLONParserRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__3__Impl"


    // $ANTLR start "rule__DataInput__Group__4"
    // InternalMML.g:894:1: rule__DataInput__Group__4 : rule__DataInput__Group__4__Impl ;
    public final void rule__DataInput__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:898:1: ( rule__DataInput__Group__4__Impl )
            // InternalMML.g:899:2: rule__DataInput__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataInput__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__4"


    // $ANTLR start "rule__DataInput__Group__4__Impl"
    // InternalMML.g:905:1: rule__DataInput__Group__4__Impl : ( ruleNEWLINE ) ;
    public final void rule__DataInput__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:909:1: ( ( ruleNEWLINE ) )
            // InternalMML.g:910:1: ( ruleNEWLINE )
            {
            // InternalMML.g:910:1: ( ruleNEWLINE )
            // InternalMML.g:911:2: ruleNEWLINE
            {
             before(grammarAccess.getDataInputAccess().getNEWLINEParserRuleCall_4()); 
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;

             after(grammarAccess.getDataInputAccess().getNEWLINEParserRuleCall_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__Group__4__Impl"


    // $ANTLR start "rule__CSVSeparator__Group__0"
    // InternalMML.g:921:1: rule__CSVSeparator__Group__0 : rule__CSVSeparator__Group__0__Impl rule__CSVSeparator__Group__1 ;
    public final void rule__CSVSeparator__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:925:1: ( rule__CSVSeparator__Group__0__Impl rule__CSVSeparator__Group__1 )
            // InternalMML.g:926:2: rule__CSVSeparator__Group__0__Impl rule__CSVSeparator__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__CSVSeparator__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CSVSeparator__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CSVSeparator__Group__0"


    // $ANTLR start "rule__CSVSeparator__Group__0__Impl"
    // InternalMML.g:933:1: rule__CSVSeparator__Group__0__Impl : ( 'separator' ) ;
    public final void rule__CSVSeparator__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:937:1: ( ( 'separator' ) )
            // InternalMML.g:938:1: ( 'separator' )
            {
            // InternalMML.g:938:1: ( 'separator' )
            // InternalMML.g:939:2: 'separator'
            {
             before(grammarAccess.getCSVSeparatorAccess().getSeparatorKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCSVSeparatorAccess().getSeparatorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CSVSeparator__Group__0__Impl"


    // $ANTLR start "rule__CSVSeparator__Group__1"
    // InternalMML.g:948:1: rule__CSVSeparator__Group__1 : rule__CSVSeparator__Group__1__Impl ;
    public final void rule__CSVSeparator__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:952:1: ( rule__CSVSeparator__Group__1__Impl )
            // InternalMML.g:953:2: rule__CSVSeparator__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CSVSeparator__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CSVSeparator__Group__1"


    // $ANTLR start "rule__CSVSeparator__Group__1__Impl"
    // InternalMML.g:959:1: rule__CSVSeparator__Group__1__Impl : ( ( rule__CSVSeparator__SeparatorAssignment_1 ) ) ;
    public final void rule__CSVSeparator__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:963:1: ( ( ( rule__CSVSeparator__SeparatorAssignment_1 ) ) )
            // InternalMML.g:964:1: ( ( rule__CSVSeparator__SeparatorAssignment_1 ) )
            {
            // InternalMML.g:964:1: ( ( rule__CSVSeparator__SeparatorAssignment_1 ) )
            // InternalMML.g:965:2: ( rule__CSVSeparator__SeparatorAssignment_1 )
            {
             before(grammarAccess.getCSVSeparatorAccess().getSeparatorAssignment_1()); 
            // InternalMML.g:966:2: ( rule__CSVSeparator__SeparatorAssignment_1 )
            // InternalMML.g:966:3: rule__CSVSeparator__SeparatorAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CSVSeparator__SeparatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCSVSeparatorAccess().getSeparatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CSVSeparator__Group__1__Impl"


    // $ANTLR start "rule__MLChoice__Group__0"
    // InternalMML.g:975:1: rule__MLChoice__Group__0 : rule__MLChoice__Group__0__Impl rule__MLChoice__Group__1 ;
    public final void rule__MLChoice__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:979:1: ( rule__MLChoice__Group__0__Impl rule__MLChoice__Group__1 )
            // InternalMML.g:980:2: rule__MLChoice__Group__0__Impl rule__MLChoice__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__MLChoice__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__0"


    // $ANTLR start "rule__MLChoice__Group__0__Impl"
    // InternalMML.g:987:1: rule__MLChoice__Group__0__Impl : ( 'mlFramework' ) ;
    public final void rule__MLChoice__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:991:1: ( ( 'mlFramework' ) )
            // InternalMML.g:992:1: ( 'mlFramework' )
            {
            // InternalMML.g:992:1: ( 'mlFramework' )
            // InternalMML.g:993:2: 'mlFramework'
            {
             before(grammarAccess.getMLChoiceAccess().getMlFrameworkKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getMLChoiceAccess().getMlFrameworkKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__0__Impl"


    // $ANTLR start "rule__MLChoice__Group__1"
    // InternalMML.g:1002:1: rule__MLChoice__Group__1 : rule__MLChoice__Group__1__Impl rule__MLChoice__Group__2 ;
    public final void rule__MLChoice__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1006:1: ( rule__MLChoice__Group__1__Impl rule__MLChoice__Group__2 )
            // InternalMML.g:1007:2: rule__MLChoice__Group__1__Impl rule__MLChoice__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__MLChoice__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__1"


    // $ANTLR start "rule__MLChoice__Group__1__Impl"
    // InternalMML.g:1014:1: rule__MLChoice__Group__1__Impl : ( ( rule__MLChoice__FrameworkAssignment_1 ) ) ;
    public final void rule__MLChoice__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1018:1: ( ( ( rule__MLChoice__FrameworkAssignment_1 ) ) )
            // InternalMML.g:1019:1: ( ( rule__MLChoice__FrameworkAssignment_1 ) )
            {
            // InternalMML.g:1019:1: ( ( rule__MLChoice__FrameworkAssignment_1 ) )
            // InternalMML.g:1020:2: ( rule__MLChoice__FrameworkAssignment_1 )
            {
             before(grammarAccess.getMLChoiceAccess().getFrameworkAssignment_1()); 
            // InternalMML.g:1021:2: ( rule__MLChoice__FrameworkAssignment_1 )
            // InternalMML.g:1021:3: rule__MLChoice__FrameworkAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__MLChoice__FrameworkAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMLChoiceAccess().getFrameworkAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__1__Impl"


    // $ANTLR start "rule__MLChoice__Group__2"
    // InternalMML.g:1029:1: rule__MLChoice__Group__2 : rule__MLChoice__Group__2__Impl rule__MLChoice__Group__3 ;
    public final void rule__MLChoice__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1033:1: ( rule__MLChoice__Group__2__Impl rule__MLChoice__Group__3 )
            // InternalMML.g:1034:2: rule__MLChoice__Group__2__Impl rule__MLChoice__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__MLChoice__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__2"


    // $ANTLR start "rule__MLChoice__Group__2__Impl"
    // InternalMML.g:1041:1: rule__MLChoice__Group__2__Impl : ( ruleCOMMA ) ;
    public final void rule__MLChoice__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1045:1: ( ( ruleCOMMA ) )
            // InternalMML.g:1046:1: ( ruleCOMMA )
            {
            // InternalMML.g:1046:1: ( ruleCOMMA )
            // InternalMML.g:1047:2: ruleCOMMA
            {
             before(grammarAccess.getMLChoiceAccess().getCOMMAParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleCOMMA();

            state._fsp--;

             after(grammarAccess.getMLChoiceAccess().getCOMMAParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__2__Impl"


    // $ANTLR start "rule__MLChoice__Group__3"
    // InternalMML.g:1056:1: rule__MLChoice__Group__3 : rule__MLChoice__Group__3__Impl rule__MLChoice__Group__4 ;
    public final void rule__MLChoice__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1060:1: ( rule__MLChoice__Group__3__Impl rule__MLChoice__Group__4 )
            // InternalMML.g:1061:2: rule__MLChoice__Group__3__Impl rule__MLChoice__Group__4
            {
            pushFollow(FOLLOW_12);
            rule__MLChoice__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__3"


    // $ANTLR start "rule__MLChoice__Group__3__Impl"
    // InternalMML.g:1068:1: rule__MLChoice__Group__3__Impl : ( 'mlAlgorithm' ) ;
    public final void rule__MLChoice__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1072:1: ( ( 'mlAlgorithm' ) )
            // InternalMML.g:1073:1: ( 'mlAlgorithm' )
            {
            // InternalMML.g:1073:1: ( 'mlAlgorithm' )
            // InternalMML.g:1074:2: 'mlAlgorithm'
            {
             before(grammarAccess.getMLChoiceAccess().getMlAlgorithmKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getMLChoiceAccess().getMlAlgorithmKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__3__Impl"


    // $ANTLR start "rule__MLChoice__Group__4"
    // InternalMML.g:1083:1: rule__MLChoice__Group__4 : rule__MLChoice__Group__4__Impl rule__MLChoice__Group__5 ;
    public final void rule__MLChoice__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1087:1: ( rule__MLChoice__Group__4__Impl rule__MLChoice__Group__5 )
            // InternalMML.g:1088:2: rule__MLChoice__Group__4__Impl rule__MLChoice__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__MLChoice__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__4"


    // $ANTLR start "rule__MLChoice__Group__4__Impl"
    // InternalMML.g:1095:1: rule__MLChoice__Group__4__Impl : ( ( rule__MLChoice__AlgorithmAssignment_4 ) ) ;
    public final void rule__MLChoice__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1099:1: ( ( ( rule__MLChoice__AlgorithmAssignment_4 ) ) )
            // InternalMML.g:1100:1: ( ( rule__MLChoice__AlgorithmAssignment_4 ) )
            {
            // InternalMML.g:1100:1: ( ( rule__MLChoice__AlgorithmAssignment_4 ) )
            // InternalMML.g:1101:2: ( rule__MLChoice__AlgorithmAssignment_4 )
            {
             before(grammarAccess.getMLChoiceAccess().getAlgorithmAssignment_4()); 
            // InternalMML.g:1102:2: ( rule__MLChoice__AlgorithmAssignment_4 )
            // InternalMML.g:1102:3: rule__MLChoice__AlgorithmAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__MLChoice__AlgorithmAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getMLChoiceAccess().getAlgorithmAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__4__Impl"


    // $ANTLR start "rule__MLChoice__Group__5"
    // InternalMML.g:1110:1: rule__MLChoice__Group__5 : rule__MLChoice__Group__5__Impl rule__MLChoice__Group__6 ;
    public final void rule__MLChoice__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1114:1: ( rule__MLChoice__Group__5__Impl rule__MLChoice__Group__6 )
            // InternalMML.g:1115:2: rule__MLChoice__Group__5__Impl rule__MLChoice__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__MLChoice__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__5"


    // $ANTLR start "rule__MLChoice__Group__5__Impl"
    // InternalMML.g:1122:1: rule__MLChoice__Group__5__Impl : ( ruleSEMICOLON ) ;
    public final void rule__MLChoice__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1126:1: ( ( ruleSEMICOLON ) )
            // InternalMML.g:1127:1: ( ruleSEMICOLON )
            {
            // InternalMML.g:1127:1: ( ruleSEMICOLON )
            // InternalMML.g:1128:2: ruleSEMICOLON
            {
             before(grammarAccess.getMLChoiceAccess().getSEMICOLONParserRuleCall_5()); 
            pushFollow(FOLLOW_2);
            ruleSEMICOLON();

            state._fsp--;

             after(grammarAccess.getMLChoiceAccess().getSEMICOLONParserRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__5__Impl"


    // $ANTLR start "rule__MLChoice__Group__6"
    // InternalMML.g:1137:1: rule__MLChoice__Group__6 : rule__MLChoice__Group__6__Impl ;
    public final void rule__MLChoice__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1141:1: ( rule__MLChoice__Group__6__Impl )
            // InternalMML.g:1142:2: rule__MLChoice__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MLChoice__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__6"


    // $ANTLR start "rule__MLChoice__Group__6__Impl"
    // InternalMML.g:1148:1: rule__MLChoice__Group__6__Impl : ( ruleNEWLINE ) ;
    public final void rule__MLChoice__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1152:1: ( ( ruleNEWLINE ) )
            // InternalMML.g:1153:1: ( ruleNEWLINE )
            {
            // InternalMML.g:1153:1: ( ruleNEWLINE )
            // InternalMML.g:1154:2: ruleNEWLINE
            {
             before(grammarAccess.getMLChoiceAccess().getNEWLINEParserRuleCall_6()); 
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;

             after(grammarAccess.getMLChoiceAccess().getNEWLINEParserRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__Group__6__Impl"


    // $ANTLR start "rule__Variables__Group__0"
    // InternalMML.g:1164:1: rule__Variables__Group__0 : rule__Variables__Group__0__Impl rule__Variables__Group__1 ;
    public final void rule__Variables__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1168:1: ( rule__Variables__Group__0__Impl rule__Variables__Group__1 )
            // InternalMML.g:1169:2: rule__Variables__Group__0__Impl rule__Variables__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Variables__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__0"


    // $ANTLR start "rule__Variables__Group__0__Impl"
    // InternalMML.g:1176:1: rule__Variables__Group__0__Impl : ( 'variables' ) ;
    public final void rule__Variables__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1180:1: ( ( 'variables' ) )
            // InternalMML.g:1181:1: ( 'variables' )
            {
            // InternalMML.g:1181:1: ( 'variables' )
            // InternalMML.g:1182:2: 'variables'
            {
             before(grammarAccess.getVariablesAccess().getVariablesKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getVariablesAccess().getVariablesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__0__Impl"


    // $ANTLR start "rule__Variables__Group__1"
    // InternalMML.g:1191:1: rule__Variables__Group__1 : rule__Variables__Group__1__Impl rule__Variables__Group__2 ;
    public final void rule__Variables__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1195:1: ( rule__Variables__Group__1__Impl rule__Variables__Group__2 )
            // InternalMML.g:1196:2: rule__Variables__Group__1__Impl rule__Variables__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Variables__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__1"


    // $ANTLR start "rule__Variables__Group__1__Impl"
    // InternalMML.g:1203:1: rule__Variables__Group__1__Impl : ( 'prediction' ) ;
    public final void rule__Variables__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1207:1: ( ( 'prediction' ) )
            // InternalMML.g:1208:1: ( 'prediction' )
            {
            // InternalMML.g:1208:1: ( 'prediction' )
            // InternalMML.g:1209:2: 'prediction'
            {
             before(grammarAccess.getVariablesAccess().getPredictionKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getVariablesAccess().getPredictionKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__1__Impl"


    // $ANTLR start "rule__Variables__Group__2"
    // InternalMML.g:1218:1: rule__Variables__Group__2 : rule__Variables__Group__2__Impl rule__Variables__Group__3 ;
    public final void rule__Variables__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1222:1: ( rule__Variables__Group__2__Impl rule__Variables__Group__3 )
            // InternalMML.g:1223:2: rule__Variables__Group__2__Impl rule__Variables__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Variables__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__2"


    // $ANTLR start "rule__Variables__Group__2__Impl"
    // InternalMML.g:1230:1: rule__Variables__Group__2__Impl : ( ( rule__Variables__PredictionsAssignment_2 ) ) ;
    public final void rule__Variables__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1234:1: ( ( ( rule__Variables__PredictionsAssignment_2 ) ) )
            // InternalMML.g:1235:1: ( ( rule__Variables__PredictionsAssignment_2 ) )
            {
            // InternalMML.g:1235:1: ( ( rule__Variables__PredictionsAssignment_2 ) )
            // InternalMML.g:1236:2: ( rule__Variables__PredictionsAssignment_2 )
            {
             before(grammarAccess.getVariablesAccess().getPredictionsAssignment_2()); 
            // InternalMML.g:1237:2: ( rule__Variables__PredictionsAssignment_2 )
            // InternalMML.g:1237:3: rule__Variables__PredictionsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Variables__PredictionsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVariablesAccess().getPredictionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__2__Impl"


    // $ANTLR start "rule__Variables__Group__3"
    // InternalMML.g:1245:1: rule__Variables__Group__3 : rule__Variables__Group__3__Impl rule__Variables__Group__4 ;
    public final void rule__Variables__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1249:1: ( rule__Variables__Group__3__Impl rule__Variables__Group__4 )
            // InternalMML.g:1250:2: rule__Variables__Group__3__Impl rule__Variables__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Variables__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__3"


    // $ANTLR start "rule__Variables__Group__3__Impl"
    // InternalMML.g:1257:1: rule__Variables__Group__3__Impl : ( ruleCOMMA ) ;
    public final void rule__Variables__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1261:1: ( ( ruleCOMMA ) )
            // InternalMML.g:1262:1: ( ruleCOMMA )
            {
            // InternalMML.g:1262:1: ( ruleCOMMA )
            // InternalMML.g:1263:2: ruleCOMMA
            {
             before(grammarAccess.getVariablesAccess().getCOMMAParserRuleCall_3()); 
            pushFollow(FOLLOW_2);
            ruleCOMMA();

            state._fsp--;

             after(grammarAccess.getVariablesAccess().getCOMMAParserRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__3__Impl"


    // $ANTLR start "rule__Variables__Group__4"
    // InternalMML.g:1272:1: rule__Variables__Group__4 : rule__Variables__Group__4__Impl rule__Variables__Group__5 ;
    public final void rule__Variables__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1276:1: ( rule__Variables__Group__4__Impl rule__Variables__Group__5 )
            // InternalMML.g:1277:2: rule__Variables__Group__4__Impl rule__Variables__Group__5
            {
            pushFollow(FOLLOW_16);
            rule__Variables__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__4"


    // $ANTLR start "rule__Variables__Group__4__Impl"
    // InternalMML.g:1284:1: rule__Variables__Group__4__Impl : ( 'target' ) ;
    public final void rule__Variables__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1288:1: ( ( 'target' ) )
            // InternalMML.g:1289:1: ( 'target' )
            {
            // InternalMML.g:1289:1: ( 'target' )
            // InternalMML.g:1290:2: 'target'
            {
             before(grammarAccess.getVariablesAccess().getTargetKeyword_4()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getVariablesAccess().getTargetKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__4__Impl"


    // $ANTLR start "rule__Variables__Group__5"
    // InternalMML.g:1299:1: rule__Variables__Group__5 : rule__Variables__Group__5__Impl rule__Variables__Group__6 ;
    public final void rule__Variables__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1303:1: ( rule__Variables__Group__5__Impl rule__Variables__Group__6 )
            // InternalMML.g:1304:2: rule__Variables__Group__5__Impl rule__Variables__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__Variables__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__5"


    // $ANTLR start "rule__Variables__Group__5__Impl"
    // InternalMML.g:1311:1: rule__Variables__Group__5__Impl : ( ( rule__Variables__TargetAssignment_5 ) ) ;
    public final void rule__Variables__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1315:1: ( ( ( rule__Variables__TargetAssignment_5 ) ) )
            // InternalMML.g:1316:1: ( ( rule__Variables__TargetAssignment_5 ) )
            {
            // InternalMML.g:1316:1: ( ( rule__Variables__TargetAssignment_5 ) )
            // InternalMML.g:1317:2: ( rule__Variables__TargetAssignment_5 )
            {
             before(grammarAccess.getVariablesAccess().getTargetAssignment_5()); 
            // InternalMML.g:1318:2: ( rule__Variables__TargetAssignment_5 )
            // InternalMML.g:1318:3: rule__Variables__TargetAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Variables__TargetAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getVariablesAccess().getTargetAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__5__Impl"


    // $ANTLR start "rule__Variables__Group__6"
    // InternalMML.g:1326:1: rule__Variables__Group__6 : rule__Variables__Group__6__Impl rule__Variables__Group__7 ;
    public final void rule__Variables__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1330:1: ( rule__Variables__Group__6__Impl rule__Variables__Group__7 )
            // InternalMML.g:1331:2: rule__Variables__Group__6__Impl rule__Variables__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__Variables__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variables__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__6"


    // $ANTLR start "rule__Variables__Group__6__Impl"
    // InternalMML.g:1338:1: rule__Variables__Group__6__Impl : ( ruleSEMICOLON ) ;
    public final void rule__Variables__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1342:1: ( ( ruleSEMICOLON ) )
            // InternalMML.g:1343:1: ( ruleSEMICOLON )
            {
            // InternalMML.g:1343:1: ( ruleSEMICOLON )
            // InternalMML.g:1344:2: ruleSEMICOLON
            {
             before(grammarAccess.getVariablesAccess().getSEMICOLONParserRuleCall_6()); 
            pushFollow(FOLLOW_2);
            ruleSEMICOLON();

            state._fsp--;

             after(grammarAccess.getVariablesAccess().getSEMICOLONParserRuleCall_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__6__Impl"


    // $ANTLR start "rule__Variables__Group__7"
    // InternalMML.g:1353:1: rule__Variables__Group__7 : rule__Variables__Group__7__Impl ;
    public final void rule__Variables__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1357:1: ( rule__Variables__Group__7__Impl )
            // InternalMML.g:1358:2: rule__Variables__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variables__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__7"


    // $ANTLR start "rule__Variables__Group__7__Impl"
    // InternalMML.g:1364:1: rule__Variables__Group__7__Impl : ( ruleNEWLINE ) ;
    public final void rule__Variables__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1368:1: ( ( ruleNEWLINE ) )
            // InternalMML.g:1369:1: ( ruleNEWLINE )
            {
            // InternalMML.g:1369:1: ( ruleNEWLINE )
            // InternalMML.g:1370:2: ruleNEWLINE
            {
             before(grammarAccess.getVariablesAccess().getNEWLINEParserRuleCall_7()); 
            pushFollow(FOLLOW_2);
            ruleNEWLINE();

            state._fsp--;

             after(grammarAccess.getVariablesAccess().getNEWLINEParserRuleCall_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__Group__7__Impl"


    // $ANTLR start "rule__PredictorVariables__Group__0"
    // InternalMML.g:1380:1: rule__PredictorVariables__Group__0 : rule__PredictorVariables__Group__0__Impl rule__PredictorVariables__Group__1 ;
    public final void rule__PredictorVariables__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1384:1: ( rule__PredictorVariables__Group__0__Impl rule__PredictorVariables__Group__1 )
            // InternalMML.g:1385:2: rule__PredictorVariables__Group__0__Impl rule__PredictorVariables__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__PredictorVariables__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredictorVariables__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group__0"


    // $ANTLR start "rule__PredictorVariables__Group__0__Impl"
    // InternalMML.g:1392:1: rule__PredictorVariables__Group__0__Impl : ( ( rule__PredictorVariables__VarsAssignment_0 ) ) ;
    public final void rule__PredictorVariables__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1396:1: ( ( ( rule__PredictorVariables__VarsAssignment_0 ) ) )
            // InternalMML.g:1397:1: ( ( rule__PredictorVariables__VarsAssignment_0 ) )
            {
            // InternalMML.g:1397:1: ( ( rule__PredictorVariables__VarsAssignment_0 ) )
            // InternalMML.g:1398:2: ( rule__PredictorVariables__VarsAssignment_0 )
            {
             before(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_0()); 
            // InternalMML.g:1399:2: ( rule__PredictorVariables__VarsAssignment_0 )
            // InternalMML.g:1399:3: rule__PredictorVariables__VarsAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PredictorVariables__VarsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group__0__Impl"


    // $ANTLR start "rule__PredictorVariables__Group__1"
    // InternalMML.g:1407:1: rule__PredictorVariables__Group__1 : rule__PredictorVariables__Group__1__Impl ;
    public final void rule__PredictorVariables__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1411:1: ( rule__PredictorVariables__Group__1__Impl )
            // InternalMML.g:1412:2: rule__PredictorVariables__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PredictorVariables__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group__1"


    // $ANTLR start "rule__PredictorVariables__Group__1__Impl"
    // InternalMML.g:1418:1: rule__PredictorVariables__Group__1__Impl : ( ( rule__PredictorVariables__Group_1__0 )* ) ;
    public final void rule__PredictorVariables__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1422:1: ( ( ( rule__PredictorVariables__Group_1__0 )* ) )
            // InternalMML.g:1423:1: ( ( rule__PredictorVariables__Group_1__0 )* )
            {
            // InternalMML.g:1423:1: ( ( rule__PredictorVariables__Group_1__0 )* )
            // InternalMML.g:1424:2: ( rule__PredictorVariables__Group_1__0 )*
            {
             before(grammarAccess.getPredictorVariablesAccess().getGroup_1()); 
            // InternalMML.g:1425:2: ( rule__PredictorVariables__Group_1__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==22) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalMML.g:1425:3: rule__PredictorVariables__Group_1__0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__PredictorVariables__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getPredictorVariablesAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group__1__Impl"


    // $ANTLR start "rule__PredictorVariables__Group_1__0"
    // InternalMML.g:1434:1: rule__PredictorVariables__Group_1__0 : rule__PredictorVariables__Group_1__0__Impl rule__PredictorVariables__Group_1__1 ;
    public final void rule__PredictorVariables__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1438:1: ( rule__PredictorVariables__Group_1__0__Impl rule__PredictorVariables__Group_1__1 )
            // InternalMML.g:1439:2: rule__PredictorVariables__Group_1__0__Impl rule__PredictorVariables__Group_1__1
            {
            pushFollow(FOLLOW_14);
            rule__PredictorVariables__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PredictorVariables__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group_1__0"


    // $ANTLR start "rule__PredictorVariables__Group_1__0__Impl"
    // InternalMML.g:1446:1: rule__PredictorVariables__Group_1__0__Impl : ( '+' ) ;
    public final void rule__PredictorVariables__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1450:1: ( ( '+' ) )
            // InternalMML.g:1451:1: ( '+' )
            {
            // InternalMML.g:1451:1: ( '+' )
            // InternalMML.g:1452:2: '+'
            {
             before(grammarAccess.getPredictorVariablesAccess().getPlusSignKeyword_1_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getPredictorVariablesAccess().getPlusSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group_1__0__Impl"


    // $ANTLR start "rule__PredictorVariables__Group_1__1"
    // InternalMML.g:1461:1: rule__PredictorVariables__Group_1__1 : rule__PredictorVariables__Group_1__1__Impl ;
    public final void rule__PredictorVariables__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1465:1: ( rule__PredictorVariables__Group_1__1__Impl )
            // InternalMML.g:1466:2: rule__PredictorVariables__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PredictorVariables__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group_1__1"


    // $ANTLR start "rule__PredictorVariables__Group_1__1__Impl"
    // InternalMML.g:1472:1: rule__PredictorVariables__Group_1__1__Impl : ( ( rule__PredictorVariables__VarsAssignment_1_1 ) ) ;
    public final void rule__PredictorVariables__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1476:1: ( ( ( rule__PredictorVariables__VarsAssignment_1_1 ) ) )
            // InternalMML.g:1477:1: ( ( rule__PredictorVariables__VarsAssignment_1_1 ) )
            {
            // InternalMML.g:1477:1: ( ( rule__PredictorVariables__VarsAssignment_1_1 ) )
            // InternalMML.g:1478:2: ( rule__PredictorVariables__VarsAssignment_1_1 )
            {
             before(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_1_1()); 
            // InternalMML.g:1479:2: ( rule__PredictorVariables__VarsAssignment_1_1 )
            // InternalMML.g:1479:3: rule__PredictorVariables__VarsAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__PredictorVariables__VarsAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getPredictorVariablesAccess().getVarsAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__Group_1__1__Impl"


    // $ANTLR start "rule__Evaluation__Group__0"
    // InternalMML.g:1488:1: rule__Evaluation__Group__0 : rule__Evaluation__Group__0__Impl rule__Evaluation__Group__1 ;
    public final void rule__Evaluation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1492:1: ( rule__Evaluation__Group__0__Impl rule__Evaluation__Group__1 )
            // InternalMML.g:1493:2: rule__Evaluation__Group__0__Impl rule__Evaluation__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Evaluation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Evaluation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__0"


    // $ANTLR start "rule__Evaluation__Group__0__Impl"
    // InternalMML.g:1500:1: rule__Evaluation__Group__0__Impl : ( 'evaluation' ) ;
    public final void rule__Evaluation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1504:1: ( ( 'evaluation' ) )
            // InternalMML.g:1505:1: ( 'evaluation' )
            {
            // InternalMML.g:1505:1: ( 'evaluation' )
            // InternalMML.g:1506:2: 'evaluation'
            {
             before(grammarAccess.getEvaluationAccess().getEvaluationKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getEvaluationAccess().getEvaluationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__0__Impl"


    // $ANTLR start "rule__Evaluation__Group__1"
    // InternalMML.g:1515:1: rule__Evaluation__Group__1 : rule__Evaluation__Group__1__Impl rule__Evaluation__Group__2 ;
    public final void rule__Evaluation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1519:1: ( rule__Evaluation__Group__1__Impl rule__Evaluation__Group__2 )
            // InternalMML.g:1520:2: rule__Evaluation__Group__1__Impl rule__Evaluation__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Evaluation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Evaluation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__1"


    // $ANTLR start "rule__Evaluation__Group__1__Impl"
    // InternalMML.g:1527:1: rule__Evaluation__Group__1__Impl : ( ( rule__Evaluation__StrategyEvaluationAssignment_1 ) ) ;
    public final void rule__Evaluation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1531:1: ( ( ( rule__Evaluation__StrategyEvaluationAssignment_1 ) ) )
            // InternalMML.g:1532:1: ( ( rule__Evaluation__StrategyEvaluationAssignment_1 ) )
            {
            // InternalMML.g:1532:1: ( ( rule__Evaluation__StrategyEvaluationAssignment_1 ) )
            // InternalMML.g:1533:2: ( rule__Evaluation__StrategyEvaluationAssignment_1 )
            {
             before(grammarAccess.getEvaluationAccess().getStrategyEvaluationAssignment_1()); 
            // InternalMML.g:1534:2: ( rule__Evaluation__StrategyEvaluationAssignment_1 )
            // InternalMML.g:1534:3: rule__Evaluation__StrategyEvaluationAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Evaluation__StrategyEvaluationAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEvaluationAccess().getStrategyEvaluationAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__1__Impl"


    // $ANTLR start "rule__Evaluation__Group__2"
    // InternalMML.g:1542:1: rule__Evaluation__Group__2 : rule__Evaluation__Group__2__Impl rule__Evaluation__Group__3 ;
    public final void rule__Evaluation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1546:1: ( rule__Evaluation__Group__2__Impl rule__Evaluation__Group__3 )
            // InternalMML.g:1547:2: rule__Evaluation__Group__2__Impl rule__Evaluation__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Evaluation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Evaluation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__2"


    // $ANTLR start "rule__Evaluation__Group__2__Impl"
    // InternalMML.g:1554:1: rule__Evaluation__Group__2__Impl : ( ruleSEMICOLON ) ;
    public final void rule__Evaluation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1558:1: ( ( ruleSEMICOLON ) )
            // InternalMML.g:1559:1: ( ruleSEMICOLON )
            {
            // InternalMML.g:1559:1: ( ruleSEMICOLON )
            // InternalMML.g:1560:2: ruleSEMICOLON
            {
             before(grammarAccess.getEvaluationAccess().getSEMICOLONParserRuleCall_2()); 
            pushFollow(FOLLOW_2);
            ruleSEMICOLON();

            state._fsp--;

             after(grammarAccess.getEvaluationAccess().getSEMICOLONParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__2__Impl"


    // $ANTLR start "rule__Evaluation__Group__3"
    // InternalMML.g:1569:1: rule__Evaluation__Group__3 : rule__Evaluation__Group__3__Impl ;
    public final void rule__Evaluation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1573:1: ( rule__Evaluation__Group__3__Impl )
            // InternalMML.g:1574:2: rule__Evaluation__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Evaluation__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__3"


    // $ANTLR start "rule__Evaluation__Group__3__Impl"
    // InternalMML.g:1580:1: rule__Evaluation__Group__3__Impl : ( ( ruleNEWLINE )? ) ;
    public final void rule__Evaluation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1584:1: ( ( ( ruleNEWLINE )? ) )
            // InternalMML.g:1585:1: ( ( ruleNEWLINE )? )
            {
            // InternalMML.g:1585:1: ( ( ruleNEWLINE )? )
            // InternalMML.g:1586:2: ( ruleNEWLINE )?
            {
             before(grammarAccess.getEvaluationAccess().getNEWLINEParserRuleCall_3()); 
            // InternalMML.g:1587:2: ( ruleNEWLINE )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==13) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalMML.g:1587:3: ruleNEWLINE
                    {
                    pushFollow(FOLLOW_2);
                    ruleNEWLINE();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEvaluationAccess().getNEWLINEParserRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__Group__3__Impl"


    // $ANTLR start "rule__SimpleSplit__Group__0"
    // InternalMML.g:1596:1: rule__SimpleSplit__Group__0 : rule__SimpleSplit__Group__0__Impl rule__SimpleSplit__Group__1 ;
    public final void rule__SimpleSplit__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1600:1: ( rule__SimpleSplit__Group__0__Impl rule__SimpleSplit__Group__1 )
            // InternalMML.g:1601:2: rule__SimpleSplit__Group__0__Impl rule__SimpleSplit__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__SimpleSplit__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleSplit__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__0"


    // $ANTLR start "rule__SimpleSplit__Group__0__Impl"
    // InternalMML.g:1608:1: rule__SimpleSplit__Group__0__Impl : ( 'TrainingSet' ) ;
    public final void rule__SimpleSplit__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1612:1: ( ( 'TrainingSet' ) )
            // InternalMML.g:1613:1: ( 'TrainingSet' )
            {
            // InternalMML.g:1613:1: ( 'TrainingSet' )
            // InternalMML.g:1614:2: 'TrainingSet'
            {
             before(grammarAccess.getSimpleSplitAccess().getTrainingSetKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSimpleSplitAccess().getTrainingSetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__0__Impl"


    // $ANTLR start "rule__SimpleSplit__Group__1"
    // InternalMML.g:1623:1: rule__SimpleSplit__Group__1 : rule__SimpleSplit__Group__1__Impl rule__SimpleSplit__Group__2 ;
    public final void rule__SimpleSplit__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1627:1: ( rule__SimpleSplit__Group__1__Impl rule__SimpleSplit__Group__2 )
            // InternalMML.g:1628:2: rule__SimpleSplit__Group__1__Impl rule__SimpleSplit__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__SimpleSplit__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SimpleSplit__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__1"


    // $ANTLR start "rule__SimpleSplit__Group__1__Impl"
    // InternalMML.g:1635:1: rule__SimpleSplit__Group__1__Impl : ( 'validationPercent' ) ;
    public final void rule__SimpleSplit__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1639:1: ( ( 'validationPercent' ) )
            // InternalMML.g:1640:1: ( 'validationPercent' )
            {
            // InternalMML.g:1640:1: ( 'validationPercent' )
            // InternalMML.g:1641:2: 'validationPercent'
            {
             before(grammarAccess.getSimpleSplitAccess().getValidationPercentKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getSimpleSplitAccess().getValidationPercentKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__1__Impl"


    // $ANTLR start "rule__SimpleSplit__Group__2"
    // InternalMML.g:1650:1: rule__SimpleSplit__Group__2 : rule__SimpleSplit__Group__2__Impl ;
    public final void rule__SimpleSplit__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1654:1: ( rule__SimpleSplit__Group__2__Impl )
            // InternalMML.g:1655:2: rule__SimpleSplit__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSplit__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__2"


    // $ANTLR start "rule__SimpleSplit__Group__2__Impl"
    // InternalMML.g:1661:1: rule__SimpleSplit__Group__2__Impl : ( ( rule__SimpleSplit__ValidationPercentAssignment_2 ) ) ;
    public final void rule__SimpleSplit__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1665:1: ( ( ( rule__SimpleSplit__ValidationPercentAssignment_2 ) ) )
            // InternalMML.g:1666:1: ( ( rule__SimpleSplit__ValidationPercentAssignment_2 ) )
            {
            // InternalMML.g:1666:1: ( ( rule__SimpleSplit__ValidationPercentAssignment_2 ) )
            // InternalMML.g:1667:2: ( rule__SimpleSplit__ValidationPercentAssignment_2 )
            {
             before(grammarAccess.getSimpleSplitAccess().getValidationPercentAssignment_2()); 
            // InternalMML.g:1668:2: ( rule__SimpleSplit__ValidationPercentAssignment_2 )
            // InternalMML.g:1668:3: rule__SimpleSplit__ValidationPercentAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__SimpleSplit__ValidationPercentAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getSimpleSplitAccess().getValidationPercentAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__Group__2__Impl"


    // $ANTLR start "rule__CrossValidation__Group__0"
    // InternalMML.g:1677:1: rule__CrossValidation__Group__0 : rule__CrossValidation__Group__0__Impl rule__CrossValidation__Group__1 ;
    public final void rule__CrossValidation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1681:1: ( rule__CrossValidation__Group__0__Impl rule__CrossValidation__Group__1 )
            // InternalMML.g:1682:2: rule__CrossValidation__Group__0__Impl rule__CrossValidation__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__CrossValidation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CrossValidation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__0"


    // $ANTLR start "rule__CrossValidation__Group__0__Impl"
    // InternalMML.g:1689:1: rule__CrossValidation__Group__0__Impl : ( 'CrossValidation' ) ;
    public final void rule__CrossValidation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1693:1: ( ( 'CrossValidation' ) )
            // InternalMML.g:1694:1: ( 'CrossValidation' )
            {
            // InternalMML.g:1694:1: ( 'CrossValidation' )
            // InternalMML.g:1695:2: 'CrossValidation'
            {
             before(grammarAccess.getCrossValidationAccess().getCrossValidationKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getCrossValidationAccess().getCrossValidationKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__0__Impl"


    // $ANTLR start "rule__CrossValidation__Group__1"
    // InternalMML.g:1704:1: rule__CrossValidation__Group__1 : rule__CrossValidation__Group__1__Impl rule__CrossValidation__Group__2 ;
    public final void rule__CrossValidation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1708:1: ( rule__CrossValidation__Group__1__Impl rule__CrossValidation__Group__2 )
            // InternalMML.g:1709:2: rule__CrossValidation__Group__1__Impl rule__CrossValidation__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__CrossValidation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CrossValidation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__1"


    // $ANTLR start "rule__CrossValidation__Group__1__Impl"
    // InternalMML.g:1716:1: rule__CrossValidation__Group__1__Impl : ( 'partitionCount' ) ;
    public final void rule__CrossValidation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1720:1: ( ( 'partitionCount' ) )
            // InternalMML.g:1721:1: ( 'partitionCount' )
            {
            // InternalMML.g:1721:1: ( 'partitionCount' )
            // InternalMML.g:1722:2: 'partitionCount'
            {
             before(grammarAccess.getCrossValidationAccess().getPartitionCountKeyword_1()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getCrossValidationAccess().getPartitionCountKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__1__Impl"


    // $ANTLR start "rule__CrossValidation__Group__2"
    // InternalMML.g:1731:1: rule__CrossValidation__Group__2 : rule__CrossValidation__Group__2__Impl ;
    public final void rule__CrossValidation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1735:1: ( rule__CrossValidation__Group__2__Impl )
            // InternalMML.g:1736:2: rule__CrossValidation__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CrossValidation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__2"


    // $ANTLR start "rule__CrossValidation__Group__2__Impl"
    // InternalMML.g:1742:1: rule__CrossValidation__Group__2__Impl : ( ( rule__CrossValidation__NumberAssignment_2 ) ) ;
    public final void rule__CrossValidation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1746:1: ( ( ( rule__CrossValidation__NumberAssignment_2 ) ) )
            // InternalMML.g:1747:1: ( ( rule__CrossValidation__NumberAssignment_2 ) )
            {
            // InternalMML.g:1747:1: ( ( rule__CrossValidation__NumberAssignment_2 ) )
            // InternalMML.g:1748:2: ( rule__CrossValidation__NumberAssignment_2 )
            {
             before(grammarAccess.getCrossValidationAccess().getNumberAssignment_2()); 
            // InternalMML.g:1749:2: ( rule__CrossValidation__NumberAssignment_2 )
            // InternalMML.g:1749:3: rule__CrossValidation__NumberAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__CrossValidation__NumberAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCrossValidationAccess().getNumberAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__Group__2__Impl"


    // $ANTLR start "rule__MMLModel__InputAssignment_0"
    // InternalMML.g:1758:1: rule__MMLModel__InputAssignment_0 : ( ruleDataInput ) ;
    public final void rule__MMLModel__InputAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1762:1: ( ( ruleDataInput ) )
            // InternalMML.g:1763:2: ( ruleDataInput )
            {
            // InternalMML.g:1763:2: ( ruleDataInput )
            // InternalMML.g:1764:3: ruleDataInput
            {
             before(grammarAccess.getMMLModelAccess().getInputDataInputParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleDataInput();

            state._fsp--;

             after(grammarAccess.getMMLModelAccess().getInputDataInputParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__InputAssignment_0"


    // $ANTLR start "rule__MMLModel__MlChoiceAssignment_1"
    // InternalMML.g:1773:1: rule__MMLModel__MlChoiceAssignment_1 : ( ruleMLChoice ) ;
    public final void rule__MMLModel__MlChoiceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1777:1: ( ( ruleMLChoice ) )
            // InternalMML.g:1778:2: ( ruleMLChoice )
            {
            // InternalMML.g:1778:2: ( ruleMLChoice )
            // InternalMML.g:1779:3: ruleMLChoice
            {
             before(grammarAccess.getMMLModelAccess().getMlChoiceMLChoiceParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleMLChoice();

            state._fsp--;

             after(grammarAccess.getMMLModelAccess().getMlChoiceMLChoiceParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__MlChoiceAssignment_1"


    // $ANTLR start "rule__MMLModel__VariablesAssignment_2"
    // InternalMML.g:1788:1: rule__MMLModel__VariablesAssignment_2 : ( ruleVariables ) ;
    public final void rule__MMLModel__VariablesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1792:1: ( ( ruleVariables ) )
            // InternalMML.g:1793:2: ( ruleVariables )
            {
            // InternalMML.g:1793:2: ( ruleVariables )
            // InternalMML.g:1794:3: ruleVariables
            {
             before(grammarAccess.getMMLModelAccess().getVariablesVariablesParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleVariables();

            state._fsp--;

             after(grammarAccess.getMMLModelAccess().getVariablesVariablesParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__VariablesAssignment_2"


    // $ANTLR start "rule__MMLModel__EvaluationAssignment_3"
    // InternalMML.g:1803:1: rule__MMLModel__EvaluationAssignment_3 : ( ruleEvaluation ) ;
    public final void rule__MMLModel__EvaluationAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1807:1: ( ( ruleEvaluation ) )
            // InternalMML.g:1808:2: ( ruleEvaluation )
            {
            // InternalMML.g:1808:2: ( ruleEvaluation )
            // InternalMML.g:1809:3: ruleEvaluation
            {
             before(grammarAccess.getMMLModelAccess().getEvaluationEvaluationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEvaluation();

            state._fsp--;

             after(grammarAccess.getMMLModelAccess().getEvaluationEvaluationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MMLModel__EvaluationAssignment_3"


    // $ANTLR start "rule__DataInput__FileAssignment_1"
    // InternalMML.g:1818:1: rule__DataInput__FileAssignment_1 : ( RULE_STRING ) ;
    public final void rule__DataInput__FileAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1822:1: ( ( RULE_STRING ) )
            // InternalMML.g:1823:2: ( RULE_STRING )
            {
            // InternalMML.g:1823:2: ( RULE_STRING )
            // InternalMML.g:1824:3: RULE_STRING
            {
             before(grammarAccess.getDataInputAccess().getFileSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDataInputAccess().getFileSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__FileAssignment_1"


    // $ANTLR start "rule__DataInput__CsvSeparatorAssignment_2"
    // InternalMML.g:1833:1: rule__DataInput__CsvSeparatorAssignment_2 : ( ruleCSVSeparator ) ;
    public final void rule__DataInput__CsvSeparatorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1837:1: ( ( ruleCSVSeparator ) )
            // InternalMML.g:1838:2: ( ruleCSVSeparator )
            {
            // InternalMML.g:1838:2: ( ruleCSVSeparator )
            // InternalMML.g:1839:3: ruleCSVSeparator
            {
             before(grammarAccess.getDataInputAccess().getCsvSeparatorCSVSeparatorParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleCSVSeparator();

            state._fsp--;

             after(grammarAccess.getDataInputAccess().getCsvSeparatorCSVSeparatorParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataInput__CsvSeparatorAssignment_2"


    // $ANTLR start "rule__CSVSeparator__SeparatorAssignment_1"
    // InternalMML.g:1848:1: rule__CSVSeparator__SeparatorAssignment_1 : ( RULE_STRING ) ;
    public final void rule__CSVSeparator__SeparatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1852:1: ( ( RULE_STRING ) )
            // InternalMML.g:1853:2: ( RULE_STRING )
            {
            // InternalMML.g:1853:2: ( RULE_STRING )
            // InternalMML.g:1854:3: RULE_STRING
            {
             before(grammarAccess.getCSVSeparatorAccess().getSeparatorSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getCSVSeparatorAccess().getSeparatorSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CSVSeparator__SeparatorAssignment_1"


    // $ANTLR start "rule__MLChoice__FrameworkAssignment_1"
    // InternalMML.g:1863:1: rule__MLChoice__FrameworkAssignment_1 : ( ruleMLFramework ) ;
    public final void rule__MLChoice__FrameworkAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1867:1: ( ( ruleMLFramework ) )
            // InternalMML.g:1868:2: ( ruleMLFramework )
            {
            // InternalMML.g:1868:2: ( ruleMLFramework )
            // InternalMML.g:1869:3: ruleMLFramework
            {
             before(grammarAccess.getMLChoiceAccess().getFrameworkMLFrameworkEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleMLFramework();

            state._fsp--;

             after(grammarAccess.getMLChoiceAccess().getFrameworkMLFrameworkEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__FrameworkAssignment_1"


    // $ANTLR start "rule__MLChoice__AlgorithmAssignment_4"
    // InternalMML.g:1878:1: rule__MLChoice__AlgorithmAssignment_4 : ( ruleMLAlgorithm ) ;
    public final void rule__MLChoice__AlgorithmAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1882:1: ( ( ruleMLAlgorithm ) )
            // InternalMML.g:1883:2: ( ruleMLAlgorithm )
            {
            // InternalMML.g:1883:2: ( ruleMLAlgorithm )
            // InternalMML.g:1884:3: ruleMLAlgorithm
            {
             before(grammarAccess.getMLChoiceAccess().getAlgorithmMLAlgorithmParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleMLAlgorithm();

            state._fsp--;

             after(grammarAccess.getMLChoiceAccess().getAlgorithmMLAlgorithmParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MLChoice__AlgorithmAssignment_4"


    // $ANTLR start "rule__RegressionTree__RegressionTreeAssignment"
    // InternalMML.g:1893:1: rule__RegressionTree__RegressionTreeAssignment : ( ( 'RegressionTree' ) ) ;
    public final void rule__RegressionTree__RegressionTreeAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1897:1: ( ( ( 'RegressionTree' ) ) )
            // InternalMML.g:1898:2: ( ( 'RegressionTree' ) )
            {
            // InternalMML.g:1898:2: ( ( 'RegressionTree' ) )
            // InternalMML.g:1899:3: ( 'RegressionTree' )
            {
             before(grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0()); 
            // InternalMML.g:1900:3: ( 'RegressionTree' )
            // InternalMML.g:1901:4: 'RegressionTree'
            {
             before(grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0()); 

            }

             after(grammarAccess.getRegressionTreeAccess().getRegressionTreeRegressionTreeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RegressionTree__RegressionTreeAssignment"


    // $ANTLR start "rule__LinearRegression__LinearRegressionAssignment"
    // InternalMML.g:1912:1: rule__LinearRegression__LinearRegressionAssignment : ( ( 'LinearRegression' ) ) ;
    public final void rule__LinearRegression__LinearRegressionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1916:1: ( ( ( 'LinearRegression' ) ) )
            // InternalMML.g:1917:2: ( ( 'LinearRegression' ) )
            {
            // InternalMML.g:1917:2: ( ( 'LinearRegression' ) )
            // InternalMML.g:1918:3: ( 'LinearRegression' )
            {
             before(grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0()); 
            // InternalMML.g:1919:3: ( 'LinearRegression' )
            // InternalMML.g:1920:4: 'LinearRegression'
            {
             before(grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0()); 

            }

             after(grammarAccess.getLinearRegressionAccess().getLinearRegressionLinearRegressionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LinearRegression__LinearRegressionAssignment"


    // $ANTLR start "rule__Variables__PredictionsAssignment_2"
    // InternalMML.g:1931:1: rule__Variables__PredictionsAssignment_2 : ( rulePredictions ) ;
    public final void rule__Variables__PredictionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1935:1: ( ( rulePredictions ) )
            // InternalMML.g:1936:2: ( rulePredictions )
            {
            // InternalMML.g:1936:2: ( rulePredictions )
            // InternalMML.g:1937:3: rulePredictions
            {
             before(grammarAccess.getVariablesAccess().getPredictionsPredictionsParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            rulePredictions();

            state._fsp--;

             after(grammarAccess.getVariablesAccess().getPredictionsPredictionsParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__PredictionsAssignment_2"


    // $ANTLR start "rule__Variables__TargetAssignment_5"
    // InternalMML.g:1946:1: rule__Variables__TargetAssignment_5 : ( ruleTarget ) ;
    public final void rule__Variables__TargetAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1950:1: ( ( ruleTarget ) )
            // InternalMML.g:1951:2: ( ruleTarget )
            {
            // InternalMML.g:1951:2: ( ruleTarget )
            // InternalMML.g:1952:3: ruleTarget
            {
             before(grammarAccess.getVariablesAccess().getTargetTargetParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleTarget();

            state._fsp--;

             after(grammarAccess.getVariablesAccess().getTargetTargetParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variables__TargetAssignment_5"


    // $ANTLR start "rule__DefaultTarget__DefaultAssignment"
    // InternalMML.g:1961:1: rule__DefaultTarget__DefaultAssignment : ( ( 'default' ) ) ;
    public final void rule__DefaultTarget__DefaultAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1965:1: ( ( ( 'default' ) ) )
            // InternalMML.g:1966:2: ( ( 'default' ) )
            {
            // InternalMML.g:1966:2: ( ( 'default' ) )
            // InternalMML.g:1967:3: ( 'default' )
            {
             before(grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0()); 
            // InternalMML.g:1968:3: ( 'default' )
            // InternalMML.g:1969:4: 'default'
            {
             before(grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0()); 

            }

             after(grammarAccess.getDefaultTargetAccess().getDefaultDefaultKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DefaultTarget__DefaultAssignment"


    // $ANTLR start "rule__NamedTarget__TargetAssignment"
    // InternalMML.g:1980:1: rule__NamedTarget__TargetAssignment : ( ruleItem ) ;
    public final void rule__NamedTarget__TargetAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1984:1: ( ( ruleItem ) )
            // InternalMML.g:1985:2: ( ruleItem )
            {
            // InternalMML.g:1985:2: ( ruleItem )
            // InternalMML.g:1986:3: ruleItem
            {
             before(grammarAccess.getNamedTargetAccess().getTargetItemParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getNamedTargetAccess().getTargetItemParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NamedTarget__TargetAssignment"


    // $ANTLR start "rule__AllVariables__AllAssignment"
    // InternalMML.g:1995:1: rule__AllVariables__AllAssignment : ( ( 'all' ) ) ;
    public final void rule__AllVariables__AllAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:1999:1: ( ( ( 'all' ) ) )
            // InternalMML.g:2000:2: ( ( 'all' ) )
            {
            // InternalMML.g:2000:2: ( ( 'all' ) )
            // InternalMML.g:2001:3: ( 'all' )
            {
             before(grammarAccess.getAllVariablesAccess().getAllAllKeyword_0()); 
            // InternalMML.g:2002:3: ( 'all' )
            // InternalMML.g:2003:4: 'all'
            {
             before(grammarAccess.getAllVariablesAccess().getAllAllKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getAllVariablesAccess().getAllAllKeyword_0()); 

            }

             after(grammarAccess.getAllVariablesAccess().getAllAllKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AllVariables__AllAssignment"


    // $ANTLR start "rule__PredictorVariables__VarsAssignment_0"
    // InternalMML.g:2014:1: rule__PredictorVariables__VarsAssignment_0 : ( ruleItem ) ;
    public final void rule__PredictorVariables__VarsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2018:1: ( ( ruleItem ) )
            // InternalMML.g:2019:2: ( ruleItem )
            {
            // InternalMML.g:2019:2: ( ruleItem )
            // InternalMML.g:2020:3: ruleItem
            {
             before(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__VarsAssignment_0"


    // $ANTLR start "rule__PredictorVariables__VarsAssignment_1_1"
    // InternalMML.g:2029:1: rule__PredictorVariables__VarsAssignment_1_1 : ( ruleItem ) ;
    public final void rule__PredictorVariables__VarsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2033:1: ( ( ruleItem ) )
            // InternalMML.g:2034:2: ( ruleItem )
            {
            // InternalMML.g:2034:2: ( ruleItem )
            // InternalMML.g:2035:3: ruleItem
            {
             before(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleItem();

            state._fsp--;

             after(grammarAccess.getPredictorVariablesAccess().getVarsItemParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PredictorVariables__VarsAssignment_1_1"


    // $ANTLR start "rule__Item__ColumnNameAssignment"
    // InternalMML.g:2044:1: rule__Item__ColumnNameAssignment : ( RULE_STRING ) ;
    public final void rule__Item__ColumnNameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2048:1: ( ( RULE_STRING ) )
            // InternalMML.g:2049:2: ( RULE_STRING )
            {
            // InternalMML.g:2049:2: ( RULE_STRING )
            // InternalMML.g:2050:3: RULE_STRING
            {
             before(grammarAccess.getItemAccess().getColumnNameSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getItemAccess().getColumnNameSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Item__ColumnNameAssignment"


    // $ANTLR start "rule__Evaluation__StrategyEvaluationAssignment_1"
    // InternalMML.g:2059:1: rule__Evaluation__StrategyEvaluationAssignment_1 : ( ruleStrategyEvaluation ) ;
    public final void rule__Evaluation__StrategyEvaluationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2063:1: ( ( ruleStrategyEvaluation ) )
            // InternalMML.g:2064:2: ( ruleStrategyEvaluation )
            {
            // InternalMML.g:2064:2: ( ruleStrategyEvaluation )
            // InternalMML.g:2065:3: ruleStrategyEvaluation
            {
             before(grammarAccess.getEvaluationAccess().getStrategyEvaluationStrategyEvaluationParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStrategyEvaluation();

            state._fsp--;

             after(grammarAccess.getEvaluationAccess().getStrategyEvaluationStrategyEvaluationParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Evaluation__StrategyEvaluationAssignment_1"


    // $ANTLR start "rule__SimpleSplit__ValidationPercentAssignment_2"
    // InternalMML.g:2074:1: rule__SimpleSplit__ValidationPercentAssignment_2 : ( RULE_INT ) ;
    public final void rule__SimpleSplit__ValidationPercentAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2078:1: ( ( RULE_INT ) )
            // InternalMML.g:2079:2: ( RULE_INT )
            {
            // InternalMML.g:2079:2: ( RULE_INT )
            // InternalMML.g:2080:3: RULE_INT
            {
             before(grammarAccess.getSimpleSplitAccess().getValidationPercentINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getSimpleSplitAccess().getValidationPercentINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleSplit__ValidationPercentAssignment_2"


    // $ANTLR start "rule__CrossValidation__NumberAssignment_2"
    // InternalMML.g:2089:1: rule__CrossValidation__NumberAssignment_2 : ( RULE_INT ) ;
    public final void rule__CrossValidation__NumberAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMML.g:2093:1: ( ( RULE_INT ) )
            // InternalMML.g:2094:2: ( RULE_INT )
            {
            // InternalMML.g:2094:2: ( RULE_INT )
            // InternalMML.g:2095:3: RULE_INT
            {
             before(grammarAccess.getCrossValidationAccess().getNumberINTTerminalRuleCall_2_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getCrossValidationAccess().getNumberINTTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CrossValidation__NumberAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000011000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000030000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000080000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000000C0000010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000005000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000008000000L});

}