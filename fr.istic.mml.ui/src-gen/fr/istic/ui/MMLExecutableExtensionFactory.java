/*
 * generated by Xtext 2.19.0
 */
package fr.istic.ui;

import com.google.inject.Injector;
import fr.istic.mml.ui.internal.MmlActivator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class MMLExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return Platform.getBundle(MmlActivator.PLUGIN_ID);
	}
	
	@Override
	protected Injector getInjector() {
		MmlActivator activator = MmlActivator.getInstance();
		return activator != null ? activator.getInjector(MmlActivator.FR_ISTIC_MML) : null;
	}

}
